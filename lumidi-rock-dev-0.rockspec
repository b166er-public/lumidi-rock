rockspec_format = "3.0"
package = "lumidi-rock"
version = "dev-0"
source = {
   url = "git+ssh://git@gitlab.com/b166er-public/lumidi-rock.git"
}
description = {
   summary = "Getting started",
   detailed = [[I will pass some more details later]],
   homepage = "https://gitlab.com/b166er-public/lumidi-rock",
   license = "MIT"
}
dependencies = {
  "lua >= 5.4"
}

external_dependencies = {
   ["RT_MIDI"] = {
      library = "rtmidi"
   }
}

build = {
   type = "builtin",
   modules = {
      ["lumidi"] = "src/main.lua",
      ["lumidi_engine_api"] = "src/engine_api.lua",
      ["lumidi_engine_config"] = "src/engine_config.lua",
      ["lumidi_engine"] = "src/engine.lua",
      ["lumidi_gate"] = "src/gate.lua",
      ["lumidi_gate_and_collection"] = "src/gate_and_collection.lua",
      ["lumidi_gate_or_collection"] = "src/gate_or_collection.lua",
      ["lumidi_chord_utils"] = "src/chord_utils.lua",
      ["lumidi_note_utils"] = "src/note_utils.lua",
      ["lumidi_sequence_layer"] = "src/sequence_layer.lua",
      ["lumidi_native_info"] = {
         sources = {
            "src/info.cpp",
            "src/info-lua-integration.cpp",
         },
         libraries = {"rtmidi"},
         incdirs = {"$(RT_MIDI_INCDIR)"},
         libdirs = {"$(RT_MIDI_LIBDIR)"}
      },
      ["lumidi_native_midi_in_device"] = {
         sources = {
            "src/midi-in-device.cpp",
            "src/midi-in-device-lua-integration.cpp",
            "src/model.cpp",
            "src/logger.cpp",
            "src/info.cpp"
         },
         libraries = {"rtmidi"},
         incdirs = {"$(RT_MIDI_INCDIR)"},
         libdirs = {"$(RT_MIDI_LIBDIR)"}
      },
      ["lumidi_native_midi_out_device"] = {
         sources = {
            "src/midi-out-device.cpp",
            "src/midi-out-device-lua-integration.cpp",
            "src/model.cpp",
            "src/logger.cpp",
            "src/info.cpp"
         },
         libraries = {"rtmidi"},
         incdirs = {"$(RT_MIDI_INCDIR)"},
         libdirs = {"$(RT_MIDI_LIBDIR)"}
      }
   }
}
