#include "info.hpp"

#include <csignal>

#include <lua.hpp>

static int li_get_input_device_count (lua_State *L) {
  int device_count = lumidi::info::get_input_device_count();
  lua_pushinteger(L, device_count);
  return 1;
}

static int li_get_output_device_count (lua_State *L) {
  int device_count = lumidi::info::get_output_device_count();
  lua_pushinteger(L, device_count);
  return 1;
}

static int li_get_input_device_names (lua_State *L) {
  std::vector<std::string> device_names = lumidi::info::get_input_device_names();

  lua_createtable (L, device_names.size(), 0);

  for(int dn = 0; dn < device_names.size(); dn++) {
    lua_pushinteger(L, dn);
    lua_pushstring(L, device_names[dn].c_str());
    lua_settable(L, 1);
  }

  return 1;
}

static int li_get_output_device_names(lua_State *L)
{
  std::vector<std::string> device_names = lumidi::info::get_output_device_names();

  lua_createtable (L, device_names.size(), 0);

  for(int dn = 0; dn < device_names.size(); dn++) {
    lua_pushinteger(L, dn);
    lua_pushstring(L, device_names[dn].c_str());
    lua_settable(L, 1);
  }

  return 1;
}

static int li_get_timestamp (lua_State *L) {
  unsigned int now = (unsigned int) lumidi::info::get_timestamp();
  lua_pushinteger(L, now);
  return 1;
}

static int li_has_caught_sigint_event (lua_State *L) {
  bool has_caught = lumidi::info::has_caught_sigint_event();
  lua_pushboolean(L, has_caught);
  return 1;
}

static int li_sleep (lua_State *L) {
  int ms = (int)luaL_checkinteger(L, 1);
  lumidi::info::sleep(ms);
  return 0;
}


static const luaL_Reg lumidi_native_info_functions[] = {
  {"get_input_device_count",   li_get_input_device_count},
  {"get_output_device_count",   li_get_output_device_count},
  {"get_input_device_names",   li_get_input_device_names},
  {"get_output_device_names",   li_get_output_device_names},
  {"get_timestamp",   li_get_timestamp},
  {"has_caught_sigint_event", li_has_caught_sigint_event},
  {"sleep", li_sleep},
  {NULL, NULL}
};


extern "C" int luaopen_lumidi_native_info (lua_State *L) {
  // Register the SIGINT handler
  //std::signal(SIGINT, lumidi::info::sigint_handler);

  luaL_newlib(L, lumidi_native_info_functions);
  return 1;
}