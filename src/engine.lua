local EXPORTS = {};

local info = require("lumidi_native_info");
local engine_config = require("lumidi_engine_config");
local midi_in_device = require("lumidi_native_midi_in_device");
local midi_out_device = require("lumidi_native_midi_out_device");

local Engine = {
    __name = "Engine",
}

function Engine:new(config)
    local obj = {};

    self.__index = self;
    setmetatable(obj, self);

    obj.config = config;
    obj.midi_dev_in = {};
    obj.midi_dev_out = {};
    obj.layer_context = {};
    obj.layer_context_fresh = {};
    obj.tick = 0;
    obj.termination_requested = false;
    return obj;
end

-- MidiInDevices
function Engine:openMidiInDevices()
    for alias,port_id in pairs(self.config.midi_dev_in) do
        self.midi_dev_in[alias] = midi_in_device.create(port_id)
        self.midi_dev_in[alias]:open();
        print("Engine: Opened MIDI IN [ " .. alias .. " ]!");
    end
end

function Engine:closeMidiInDevices()
    for alias,handler in pairs(self.midi_dev_in) do
        handler:close();
        print("Engine: Closed MIDI IN [ " .. alias .. " ]!");
    end
end

-- MidiOutDevices
function Engine:openMidiOutDevices()
    for alias,port_id in pairs(self.config.midi_dev_out) do
        self.midi_dev_out[alias] = midi_out_device.create(port_id)
        self.midi_dev_out[alias]:open();
        print("Engine: Opened MIDI OUT [ " .. alias .. " ]!");
    end
end

function Engine:closeMidiOutDevices()
    for alias,handler in pairs(self.midi_dev_out) do
        handler:close();
        print("Engine: Closed MIDI OUT [ " .. alias .. " ]!");
    end
end

function Engine:loadLayers()
    for index, fn_info in ipairs(self.config.layers) do
        table.insert(
            self.layer_context,
            {
                name = fn_info.name,
                cr = coroutine.create(fn_info.fn),
                wait = 0,
                alive = true
            }
        );
    end
end

function Engine:loadMasterClockLayer()
    if self.config.master_clock then
        local mc_fn = function (api)
            local tpp = api:get_tpb() / 24;
            while true do
                for alias, port in pairs(api:get_outputs()) do
                    port:send_clock();
                end
                api:sleep(tpp);
            end
        end

        table.insert(
            self.layer_context,
            {
                name = "master-clock",
                cr = coroutine.create(mc_fn),
                wait = 0,
                alive = true
            }
        );

    end
end

function Engine:loadTerminationLayer()
    local termination_fn = function (api)
        local tpp = api:get_tpb() / 24;
        while true do
            for alias, port in pairs(api:get_inputs()) do
                if port:is_stopped() then
                    api:request_stop();
                end
            end
            api:sleep(tpp);
        end
    end

    table.insert(
        self.layer_context,
        {
            name = "termination-layer",
            cr = coroutine.create(termination_fn),
            wait = 0,
            alive = true
        }
    );
end

function Engine:process()
    -- Create API
    -- NOTE : Do not put the require to the top as you might get enless recursion
    local api = require("lumidi_engine_api").create(self);

    -- Calculate ns per tick
    local us_per_s = 1000000;
    local us_per_m = us_per_s * 60;
    local us_per_beat = us_per_m / self.config.bpm;
    local us_per_tick = us_per_beat / self.config.tpb;

    local ticks_per_5s = math.floor(5000000 / us_per_tick);

    local last_tick = info.get_timestamp();

    while not self.termination_requested do
        -- Wait until it is time for next tick
        local utilization = 0;
        local free_calls = 1.0;
        while true do
            local now = info.get_timestamp();
            if (now - last_tick) >= us_per_tick then
                self.tick = self.tick + 1;
                last_tick = last_tick + us_per_tick;
                break;
            end
            free_calls = free_calls + 1.0;
        end

        -- Compute utilization
        utilization = ( utilization + (1.0 / free_calls) ) / 2.0;
        free_calls = 1.0;
        if self.tick % ticks_per_5s == 0 then
            print("Utilization -> " .. (utilization*100.0) .. "%");
        end

        -- Execute contexts which needs to activated (first time)
        local found_dead_context = false;

        local process_single_context = function (context)
            if context.wait == 0 then
                local no_error, wait_ticks = coroutine.resume(context.cr, api);

                if coroutine.status(context.cr) == "dead" then
                    if not no_error then
                        if wait_ticks then
                            print("Couroutine [ " .. context.name .. " ] died -> " .. wait_ticks);
                        end
                    end
                    context.wait = -1;
                    context.alive = false;
                    found_dead_context = true;
                else
                    if no_error then
                        context.wait = math.floor(wait_ticks);
                    else
                        if wait_ticks then
                            print("Couroutine [ " .. context.name .. " ] died -> " .. wait_ticks);
                        end
                        context.wait = -1;
                        context.alive = false;
                        found_dead_context = true;
                    end
                end
            else
                context.wait = context.wait - 1;
            end
        end

        for _, ctx in ipairs(self.layer_context) do
            process_single_context(ctx);
        end

        -- Execute fresh contexts (if created)
        while #self.layer_context_fresh > 0 do
            local new_ctx = table.remove(self.layer_context_fresh,1);
            table.insert( self.layer_context, new_ctx );
            process_single_context(new_ctx);
        end

        -- Remove dead contexts
        while found_dead_context do
            for index, ctx in ipairs(self.layer_context) do
                if ctx.alive == false then
                    table.remove(self.layer_context,index);
                    goto continue;
                end
            end
            found_dead_context = false;
            ::continue::
        end
    end
end

function Engine:get_timestamp()
    return midi_in_device.get_timestamp();
end

function Engine:start()
    print("Engine started!");

    -- Open MIDI IN and MIDI OUT
    self:openMidiInDevices();
    self:openMidiOutDevices();

    -- Check if there is MIDI IN and MIDI OUT opened
    local is_any_midi_in_open = not (next(self.midi_dev_in)==nil);
    local is_any_midi_out_open = not (next(self.midi_dev_out)==nil);
    local midi_setup_valid = (is_any_midi_in_open) and (is_any_midi_out_open);

    if not midi_setup_valid then
        error("The MIDI setup is invalid for execution!");
    end

    -- Load layers
    self:loadLayers();

    -- Load master clock layer
    self:loadMasterClockLayer();

    -- Load termination layer
    self:loadTerminationLayer();

    -- Start processing
    local ok, msg = pcall(Engine.process, self);
    if not ok then
        print("Engine ERROR : " .. msg);
    end
    -- Processing stopped

    -- Close MIDI IN and MIDI OUT
    self:closeMidiOutDevices();
    self:closeMidiInDevices();

    print("Engine terminated!");
end

function EXPORTS.create(config)
    if engine_config.instanceof(config) then
        return Engine:new(config)
    else
        error("Please provide an instance of EngineConfig as configuration!");
    end
end

function EXPORTS.instanceof(obj)
    return getmetatable(obj) == Engine;
end

return EXPORTS;