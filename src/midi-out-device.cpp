#include "midi-out-device.hpp"

#include "info.hpp"

#include <sstream> 

namespace lumidi {
namespace devices {

MidiOutDevice::MidiOutDevice(unsigned int port_id) {
    this->port_id = port_id;

    std::stringstream logger_name_stream;
    logger_name_stream << "MidiOutDevice";
    logger_name_stream << " [ " << (this->port_id) << " ]";

    this->logger = std::unique_ptr<lumidi::utils::Logger>(
        new lumidi::utils::Logger(
            logger_name_stream.str()
        )
    );
    this->rt_port_handler = nullptr;
    this->is_opened = false;
}

MidiOutDevice::~MidiOutDevice()
{
    this->key_callbacks.clear();
    this->controller_callbacks.clear();

    if(this->rt_port_handler) {
        try {
            this->rt_port_handler->closePort();
        }
        catch ( RtMidiError &error ) {
            this->logger->error("Could not destroy device!");
            error.printMessage();
        }
        this->rt_port_handler.reset();
    }
}

unsigned int MidiOutDevice::get_port_id(void)
{
    return this->port_id;
}

bool MidiOutDevice::open(void)
{
    if(this->is_opened) {
        this->logger->error("Can not open already opened device!");
    } else {
        try {
            this->rt_port_handler = std::unique_ptr<RtMidiOut>(new RtMidiOut());
            this->rt_port_handler->openPort(this->port_id);
            
            this->key_callbacks.insert(
                this->key_callbacks.begin(), 
                default_key_cb
            );
            
            this->controller_callbacks.insert(
                this->controller_callbacks.begin(), 
                default_controller_cb
            );

            this->pitch_bend_callbacks.insert(
                this->pitch_bend_callbacks.begin(),
                default_pitch_bend_cb
            );
            
            this->is_opened = true; return true;
        }
        catch ( RtMidiError &error ) {
            this->logger->error("Could not open MidiOutDevice!");
            error.printMessage();
        }
    }
    return false;
}

bool MidiOutDevice::close(void)
{
    if(this->is_opened) {
        try {
            // Switch off yet active notes
            unsigned long ts = lumidi::info::get_timestamp();
            for(unsigned char chn = 0; chn < NUMBER_OF_CHANNELS; chn++) {
                for(unsigned char key = 0; key < NUMBER_OF_KEYS_PER_CHANNEL; key++) {
                    this->set_key_value(chn, key, 0);
                }
            }

            this->key_callbacks.erase(this->key_callbacks.begin());
            this->controller_callbacks.erase(this->controller_callbacks.begin());
            this->pitch_bend_callbacks.erase(this->pitch_bend_callbacks.begin());

            this->rt_port_handler->closePort();
            this->rt_port_handler.reset();
            this->is_opened = false; return true;
        }
        catch ( RtMidiError &error ) {
            this->logger->error("Could not close MidiOutDevice!");
            error.printMessage();
        }
    } else {
        this->logger->error("Can not closed a non opened device!");
    }

    return false;
}

void MidiOutDevice::set_key_value(
    ChannelId chn_id, 
    KeyId key_id, 
    KeyValue key_value 
)
{
    unsigned long ts = lumidi::info::get_timestamp();
    if( this->state.set_key_value(ts, chn_id, key_id, key_value) ) {
        for(KeyChangedCallback clbk : this->key_callbacks) {
            clbk( ts, chn_id, key_id, key_value, (void*) this );
        }
    }
}

void MidiOutDevice::set_controller_value(
    ChannelId chn_id, 
    ControllerId ctrl_id, 
    ControllerValue ctrl_value
)
{
    unsigned long ts = lumidi::info::get_timestamp();
    if(this->state.set_controller_value(ts, chn_id, ctrl_id, ctrl_value) ) {
        for(ControllerChangedCallback clbk : this->controller_callbacks) {
            clbk( ts, chn_id, ctrl_id, ctrl_value, (void*) this);
        }
    }
}

void MidiOutDevice::set_pitch_bend_value(
    ChannelId chn_id, 
    HighResolutionValue pb_value
)
{
    unsigned long ts = lumidi::info::get_timestamp();
    if(this->state.set_pitch_bend_value(ts, chn_id, pb_value) ) {
        for(PitchBendChangedCallback clbk : this->pitch_bend_callbacks) {
            clbk( ts, chn_id, pb_value, (void*) this);
        }
    }
}

void 
MidiOutDevice::set_high_resolution_controller_value(
    ChannelId chn_id, 
    HighResolutionControllerId hi_ctrl_id, 
    HighResolutionValue hi_ctrl_value
)
{
    unsigned long ts = lumidi::info::get_timestamp();
    std::pair<bool, bool> changed = this->state.set_high_resolution_controller_value(
        ts, 
        chn_id, 
        hi_ctrl_id, 
        hi_ctrl_value
    );

    // MSB
    if(changed.first) {
        ControllerId ctrl_id_msb = hi_ctrl_id;
        ControllerValue ctrl_msb_value = this->state.get_controller_value(chn_id,ctrl_id_msb);
        for(ControllerChangedCallback clbk : this->controller_callbacks) {
            clbk( ts, chn_id, ctrl_id_msb, ctrl_msb_value, (void*) this);
        }
    }

    // LSB
    if(changed.first || changed.second) {
        ControllerId ctrl_id_lsb = hi_ctrl_id + NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL;
        ControllerValue ctrl_lsb_value = this->state.get_controller_value(chn_id,ctrl_id_lsb);
        for(ControllerChangedCallback clbk : this->controller_callbacks) {
            clbk( ts, chn_id, ctrl_id_lsb, ctrl_lsb_value, (void*) this);
        }
    }
}

void MidiOutDevice::send_clock(void)
{
    try {
        unsigned char msg[1];
        msg[0] = 0xF8;
        this->rt_port_handler->sendMessage( ((unsigned char*)&msg), std::size(msg) );
    }
    catch ( RtMidiError &error ) {
        this->logger->error("Could not send clock tick!");
        error.printMessage();
    }
}

void MidiOutDevice::send_start(void)
{
    try {
        unsigned char msg[1];
        msg[0] = 0xFA;
        this->rt_port_handler->sendMessage( ((unsigned char*)&msg), std::size(msg) );
    }
    catch ( RtMidiError &error ) {
        this->logger->error("Could not send start message!");
        error.printMessage();
    }
}

void MidiOutDevice::send_stop(void)
{
    try {
        unsigned char msg[1];
        msg[0] = 0xFC;
        this->rt_port_handler->sendMessage( ((unsigned char*)&msg), std::size(msg) );
    }
    catch ( RtMidiError &error ) {
        this->logger->error("Could not send stop message!");
        error.printMessage();
    }
}

void MidiOutDevice::default_key_cb(
        Timestamp ts, 
        ChannelId chn_id, 
        KeyId key_id, 
        KeyValue key_value,
        void* user_data
)
{
    MidiOutDevice* self = (MidiOutDevice*) user_data;
    if(self) {
        try {
            unsigned char msg[3];
            
            if(key_value == 0) {
                msg[0] = 0x80;
            } else {
                msg[0] = 0x90;
            }

            msg[0] |= (((unsigned char)chn_id) & 0x0F);
            msg[1] = ((unsigned char) key_id) & 0x7F;
            msg[2] = ((unsigned char) key_value) & 0x7F;
            
            self->rt_port_handler->sendMessage( ((unsigned char*)&msg), std::size(msg) );
        }
        catch ( RtMidiError &error ) {
            self->logger->error("Could not send key message!");
            error.printMessage();
        }
        
    } else {
        std::cout << "ERROR : Key callback was wrongly initialized!" << std::endl;
    }
}

void MidiOutDevice::default_controller_cb(
    Timestamp ts, 
    ChannelId chn_id, 
    ControllerId ctrl_id, 
    ControllerValue ctrl_value,
    void* user_data
)
{
    MidiOutDevice* self = (MidiOutDevice*) user_data;
    if(self) {
        try {
            unsigned char msg[3];

            msg[0] = 0xB0 | (((unsigned char)chn_id) & 0x0F);
            msg[1] = ((unsigned char) ctrl_id) & 0x7F;
            msg[2] = ((unsigned char) ctrl_value) & 0x7F;
            
            self->rt_port_handler->sendMessage( ((unsigned char*)&msg), std::size(msg) );
        }
        catch ( RtMidiError &error ) {
            self->logger->error("Could not send key message!");
            error.printMessage();
        }
    } else {
        std::cout << "ERROR : Controller callback was wrongly initialized!" << std::endl;
    }
}

void MidiOutDevice::default_pitch_bend_cb(
    Timestamp ts, 
    ChannelId chn_id, 
    HighResolutionValue pb_value,
    void* user_data
)
{
    MidiOutDevice* self = (MidiOutDevice*) user_data;
    if(self) {
        try {
            unsigned char msg[3];

            /* LSB calculation */
            unsigned char lsb = (unsigned char) (pb_value & 0x0000007F);

            /* MSB calculation */
            unsigned char msb = (unsigned char) ((pb_value & 0x00003F80) >> 7);

            msg[0] = 0xE0 | (((unsigned char)chn_id) & 0x0F);
            msg[1] = lsb;
            msg[2] = msb;
            
            self->rt_port_handler->sendMessage( ((unsigned char*)&msg), std::size(msg) );
        }
        catch ( RtMidiError &error ) {
            self->logger->error("Could not send pitch bend message!");
            error.printMessage();
        }
    } else {
        std::cout << "ERROR : Pitch bend callback was wrongly initialized!" << std::endl;
    }
}

void MidiOutDevice::add_key_callback(KeyChangedCallback callback)
{
    this->key_callbacks.push_back(callback);
}

void MidiOutDevice::add_controller_callback(ControllerChangedCallback callback)
{
    this->controller_callbacks.push_back(callback);
}

void MidiOutDevice::reset_key_callbacks(void)
{
    this->key_callbacks.clear();
    if(this->is_opened) { this->key_callbacks.push_back(default_key_cb); }
}

void MidiOutDevice::reset_controller_callbacks(void)
{
    this->controller_callbacks.clear();
    if(this->is_opened) { this->controller_callbacks.push_back(default_controller_cb); }
}

void MidiOutDevice::reset_pitch_bend_callbacks(void)
{
    this->pitch_bend_callbacks.clear();
    if(this->is_opened) { this->pitch_bend_callbacks.push_back(default_pitch_bend_cb); }
}

void MidiOutDevice::reset_callbacks(void)
{
    reset_key_callbacks();
    reset_controller_callbacks();
    reset_pitch_bend_callbacks();
}

}
}