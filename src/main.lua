local EXPORTS = {};

EXPORTS["Info"] = require("lumidi_native_info");
EXPORTS["MidiDeviceOut"] = require("lumidi_native_midi_out_device");
EXPORTS["MidiDeviceIn"] = require("lumidi_native_midi_in_device");
EXPORTS["EngineConfig"] = require("lumidi_engine_config");
EXPORTS["Engine"] = require("lumidi_engine");
EXPORTS["NoteUtils"] = require("lumidi_note_utils");
EXPORTS["ChordUtils"] = require("lumidi_chord_utils");
EXPORTS["SequenceLayer"] = require("lumidi_sequence_layer");
EXPORTS["Gate"] = require("lumidi_gate");
EXPORTS["GateAndCollection"] = require("lumidi_gate_and_collection");
EXPORTS["GateOrollection"] = require("lumidi_gate_or_collection");

-- Print all MIDI IN ports
dev_in = EXPORTS.Info.get_input_device_names()
dev_out = EXPORTS.Info.get_output_device_names()

print("MIDI IN Ports:");
for k,v in pairs(dev_in) do print(k .. " -> " .. v) end
print("MIDI OUT Ports:");
for k,v in pairs(dev_out) do print(k .. " -> " .. v) end

return EXPORTS;