local EXPORTS = {};

local SequenceLayer = {
    __name = "SequenceLayer",
}

function SequenceLayer:new(
    api,
    out_port_alias,
    channel,
    keys,
    durations,
    velocities,
    play_gate_auto_close,
    name
)

    local obj = {};

    self.__index = self;
    setmetatable(obj, self);

    obj.engine_api = api;
    obj.keys = keys;
    obj.durations = durations;
    obj.play_gate_auto_close = play_gate_auto_close;

    obj.play_gate = gate.create(api);
    obj.termination_gate = gate.create(api);
    obj.out_port = api:get_output_port(out_port_alias);
    obj.velocities = velocities;
    obj.channel = channel;
    obj.name = name;
    return obj;
end

function SequenceLayer:activate()
    local seq_fn = function ()

        local num_of_durations = #self.durations;
        local num_of_velocities = #self.velocities;

        local play_or_termination_gate = gate_or_collection.create(
            self.engine_api,
            {self.play_gate, self.termination_gate}
        );

        while self.termination_gate:is_closed() do
            play_or_termination_gate:wait_open();
            if self.play_gate:is_opened() then
                if self.play_gate_auto_close then
                    self.play_gate:close();
                end

                local last_activated_key = nil;

                for key_idx, key_val in ipairs(self.keys) do
                    if last_activated_key then
                        self.out_port:set_key_value(self.channel,last_activated_key,0);
                    end

                    local duration_idx = ((key_idx-1) % num_of_durations) + 1;
                    local velocity_idx = ((key_idx-1) % num_of_velocities) + 1;
                    
                    self.engine_api:sleep_beat(self.durations[duration_idx]);
                    
                    self.out_port:set_key_value(
                        self.channel, 
                        key_val, 
                        self.velocities[velocity_idx]
                    );
                    last_activated_key = key_val;
                end

                self.out_port:set_key_value(self.channel,last_activated_key,0);
            end
        end

    end

    if not self.name then
        self.name = "seq-layer-" .. math.random(10000,19999);
    end

    self.engine_api:add_layer(self.name, seq_fn);
end

function SequenceLayer:get_play_gate()
    return self.play_gate;
end

function SequenceLayer:get_termination_gate()
    return self.termination_gate;
end

gate = nil;
gate_and_collection = nil;
gate_or_collection = nil;
engine_api = nil;

function EXPORTS.create(
    engine_api_instance,
    out_port_alias,
    channel,
    keys,
    durations,
    velocities,
    play_gate_auto_close
)
    -- Load dependencies
    if not gate then
        gate = require("lumidi_gate");
    end

    if not gate_and_collection then
        gate_and_collection = require("lumidi_gate_and_collection");
    end

    if not gate_or_collection then
        gate_or_collection = require("lumidi_gate_or_collection");
    end

    if not engine_api then
        engine_api = require("lumidi_engine_api");
    end

    -- Create instance
    if engine_api.instanceof(engine_api_instance) then
        return SequenceLayer:new(
            engine_api_instance,
            out_port_alias,
            channel,
            keys,
            durations,
            velocities,
            play_gate_auto_close
        );
    else
        error("Please provide an instance of EngineApi as parameter!");
    end
end

return EXPORTS;