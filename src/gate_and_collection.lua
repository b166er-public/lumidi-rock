local EXPORTS = {};

local GateAndCollection = {
    __name = "GateAndCollection",
}

gate = nil;
engine_api = nil;

function GateAndCollection:new(engine_api_instance, gates)
    local obj = {};

    self.__index = self;
    setmetatable(obj, self);

    obj.engine_api = engine_api_instance;
    obj.gates = gates;
    return obj;
end

function GateAndCollection:is_opened()
    for _, gate_instance in ipairs(self.gates) do
        if gate_instance:is_closed() then
            return false;
        end
    end
    return true;
end

function GateAndCollection:is_closed()
    return not self:is_opened();
end

function GateAndCollection:wait_open()
    while true do
        if self:is_opened() then
            return;
        else
            self.engine_api:sleep(0);
        end
    end
end

function GateAndCollection:wait_close()
    while true do
        if self:is_closed() then
            return;
        else
            self.engine_api:sleep(0);
        end
    end
end


function EXPORTS.create(engine_api_instance, gates)
    
    if not gate then
        gate = require("lumidi_gate");
    end

    if not engine_api then
        engine_api = require("lumidi_engine_api");
    end

    if not engine_api.instanceof(engine_api_instance) then
        error("Please provide an instance of EngineApi as parameter!");
    end

    for _, gate_instance in ipairs(gates) do
        if not gate.instanceof(gate_instance) then
            error("Please provide a list consisting only of Gate instances!");    
        end
    end

    return GateAndCollection:new(engine_api_instance, gates);
end

return EXPORTS;