#include "info.hpp"

#include <memory>
#include <chrono>
#include <thread>
#include <atomic>

#include <rtmidi/RtMidi.h>

namespace lumidi {
namespace info {

unsigned int get_input_device_count(void) {
    int result = -1;

    try {
        std::unique_ptr<RtMidiIn> midiin(new RtMidiIn());
        if(midiin) {
            result = midiin->getPortCount();
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiIn handler!" << std::endl;
        error.printMessage();
    }

    return result;
}

unsigned int get_output_device_count(void) {
    int result = -1;

    try {
        std::unique_ptr<RtMidiOut> midiout(new RtMidiOut());
        if(midiout) {
            result = midiout->getPortCount();
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiOut handler!" << std::endl;
        error.printMessage();
    }

    return result;
}

std::vector<std::string> get_input_device_names(void) {
  std::vector<std::string> result;
  unsigned int device_count = get_input_device_count();
  
  try {
      std::unique_ptr<RtMidiIn> midiin(new RtMidiIn());
      if(midiin) {
        for(unsigned int d = 0; d < device_count; d++) {
          result.push_back( midiin->getPortName(d) );
        }
      }
  }
  catch ( RtMidiError &error ) {
      std::cout << "ERROR: Could not open RtMidiIn handler!" << std::endl;
      error.printMessage();
  }

  return result;
}

std::vector<std::string> get_output_device_names(void)
{
  std::vector<std::string> result;
  unsigned int device_count = get_output_device_count();
  
  try {
      std::unique_ptr<RtMidiOut> midiout(new RtMidiOut());
      if(midiout) {
        for(unsigned int d = 0; d < device_count; d++) {
          result.push_back( midiout->getPortName(d) );
        }
      }
  }
  catch ( RtMidiError &error ) {
      std::cout << "ERROR: Could not open RtMidiOut handler!" << std::endl;
      error.printMessage();
  }

  return result;
}

static std::chrono::high_resolution_clock::time_point* t0 = nullptr;

unsigned long get_timestamp(void)
{
  if(!t0) {
    t0 = (std::chrono::high_resolution_clock::time_point*) malloc(sizeof(std::chrono::high_resolution_clock::time_point));
    *t0 = std::chrono::high_resolution_clock::now();
  }

  std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::microseconds>(now - *t0).count();
}

static std::atomic<bool> sigint_caught(false);

void sigint_handler(int signal)
{
  sigint_caught = true;
}

bool has_caught_sigint_event(void)
{
  return sigint_caught;
}

void sleep(int milliseconds)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}

}
}