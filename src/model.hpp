#pragma once

#include <cstdint>

#include <vector>
#include <optional>

namespace lumidi {
namespace model {

typedef unsigned long Timestamp;

typedef unsigned char ChannelId;
typedef unsigned char ControllerId;
typedef unsigned char HighResolutionControllerId;
typedef unsigned char KeyId;

typedef uint8_t KeyValue;
typedef uint8_t ControllerValue;
typedef uint32_t HighResolutionValue;

constexpr unsigned char NUMBER_OF_CHANNELS = 16;
constexpr unsigned char NUMBER_OF_KEYS_PER_CHANNEL = 128;
constexpr unsigned char NUMBER_OF_CONTROLLERS_PER_CHANNEL = 128;
constexpr unsigned char NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL = 32;

constexpr unsigned char RANGE_KEY_ID_MIN = 0;
constexpr unsigned char RANGE_KEY_ID_MAX = NUMBER_OF_KEYS_PER_CHANNEL - 1;

constexpr unsigned char RANGE_CONTROLLER_ID_MIN = 0;
constexpr unsigned char RANGE_CONTROLLER_ID_MAX = NUMBER_OF_CONTROLLERS_PER_CHANNEL - 1;

constexpr unsigned char RANGE_HIGH_RESOLUTION_CONTROLLER_ID_MIN = 0;
constexpr unsigned char RANGE_HIGH_RESOLUTION_CONTROLLER_ID_MAX = NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL - 1;

struct ControllerState {
    Timestamp modified;
    ControllerValue value;
    bool set(Timestamp ts, ControllerValue value);
};

struct KeyState {
    Timestamp modified;
    Timestamp last_pressed;
    Timestamp last_released;
    KeyValue value;
    unsigned int activation_counter;
    bool set(Timestamp ts, KeyValue value);
};

struct PitchBend {
    Timestamp modified;
    HighResolutionValue value;
    bool set(Timestamp ts, HighResolutionValue value);
};

struct ChannelState {
    KeyState keys[NUMBER_OF_KEYS_PER_CHANNEL];
    ControllerState controllers[NUMBER_OF_CONTROLLERS_PER_CHANNEL];
    PitchBend pitch_bend;
};

enum TransportValue {
    STARTED, STOPPED, UNDEFINED
};

struct TransportState {
    Timestamp modified;
    TransportValue value = TransportValue::UNDEFINED;
    bool set(
        Timestamp ts,
        TransportValue value
    );
};

class DeviceState {
    
    ChannelState channels[NUMBER_OF_CHANNELS];
    TransportState transport;

public:
    bool set_key_value(Timestamp ts, ChannelId chn_id, KeyId key_id, KeyValue key_value);
    bool set_controller_value(Timestamp ts, ChannelId chn_id, ControllerId ctrl_id, ControllerValue ctrl_value);
    bool set_pitch_bend_value(Timestamp ts, ChannelId chn_id, HighResolutionValue value);
    std::pair<bool, bool> set_high_resolution_controller_value(
        Timestamp ts, 
        ChannelId chn_id, 
        HighResolutionControllerId hi_ctrl_id, 
        HighResolutionValue hi_ctrl_value
    );
    bool set_transport_value(Timestamp ts, TransportValue value);

    /* Query - Basic */
    ControllerValue get_controller_value(ChannelId chn_id, ControllerId ctrl_id);
    Timestamp get_controller_modified_ts(ChannelId chn_id, ControllerId ctrl_id);

    KeyValue get_key_value(ChannelId chn_id, KeyId key_id);
    Timestamp get_key_modified_ts(ChannelId chn_id, KeyId key_id);
    Timestamp get_key_last_pressed_ts(ChannelId chn_id, KeyId key_id);
    Timestamp get_key_last_released_ts(ChannelId chn_id, KeyId key_id);
    unsigned int get_key_activation_counter(ChannelId chn_id, KeyId key_id);

    HighResolutionValue get_pitch_bend_value(ChannelId chn_id);
    Timestamp get_pitch_bend_modified_ts(ChannelId chn_id);

    HighResolutionValue get_high_resolution_controller_value(ChannelId chn_id, HighResolutionControllerId hi_ctrl_id);
    Timestamp get_high_resolution_controller_modified_ts(ChannelId chn_id, HighResolutionControllerId hi_ctrl_id);

    TransportValue get_transport_value(void);
    Timestamp get_transport_value_modified_ts(void);

    /* Query - Advanced */
    std::vector<ControllerId> list_modified_controllers_after(
        ChannelId chn_id, 
        Timestamp ts, 
        ControllerId ctrl_id_min = RANGE_CONTROLLER_ID_MIN,
        ControllerId ctrl_id_max = RANGE_CONTROLLER_ID_MAX
    );
    
    std::vector<KeyId> list_modified_keys_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    std::vector<KeyId> list_activated_keys_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    std::vector<KeyId> list_released_keys_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );
    
    std::vector<KeyId> list_activated_keys_now(
        ChannelId chn_id,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    std::optional<KeyId> last_activated_key(
        ChannelId chn_id,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    bool any_key_changes_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );
    
    bool any_key_activated_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    bool any_key_released_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    bool any_controller_changes_after(
        ChannelId chn_id, 
        Timestamp ts,
        ControllerId ctrl_id_min = RANGE_CONTROLLER_ID_MIN,
        ControllerId ctrl_id_max = RANGE_CONTROLLER_ID_MAX
    );

    bool
    any_pitch_bend_changes_after(
        ChannelId chn_id, 
        Timestamp ts
    );

    bool any_changes_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX,
        ControllerId ctrl_id_min = RANGE_CONTROLLER_ID_MIN,
        ControllerId ctrl_id_max = RANGE_CONTROLLER_ID_MAX
    );

    bool is_started(void);
    bool is_stopped(void);

};

}
}