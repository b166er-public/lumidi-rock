#include "midi-in-device.hpp"

#include <memory>

#include <lua.hpp>

using namespace lumidi::devices;

static const char* METATABLE_MIDI_IN_DEVICE = "lumidi::devices::MidiInDevice";

static void push_uc_vector(lua_State *L, std::vector<unsigned char> v) {
    lua_newtable(L);
    unsigned int v_size = v.size();
    for(int i = 0; i < v_size; i++) {
        lua_pushinteger(L,v[i]);
        lua_rawseti(L, -2, i+1);
    }
}

static int mid_create (lua_State *L) {
    int port_id = (int)luaL_checkinteger(L, 1);
  
    void* midi_in_device_raw_memory = lua_newuserdata(L, sizeof(MidiInDevice));
    MidiInDevice* self = new (midi_in_device_raw_memory) MidiInDevice(port_id);

    luaL_getmetatable(L, METATABLE_MIDI_IN_DEVICE);
    lua_setmetatable(L, -2);

    return 1;
}

static int mid_get_timestamp(lua_State *L) {
    int ts = MidiInDevice::get_timestamp();
    lua_pushinteger(L,ts);
    return 1;
}

static int mid_gc (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    self->~MidiInDevice();
    return 0;
}

static int mid_open (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    if(!self->open()) {
        luaL_error(L, "Could not open MidiInDevice on port %d", self->get_port_id());
    }
    return 0;
}

static int mid_close (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    if(!self->close()) {
        luaL_error(L, "Could not close MidiInDevice on port %d", self->get_port_id());
    }
    return 0;
}

static int mid_get_controller_value (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    ControllerId ctrl_id = (int)luaL_checkinteger(L, 3);
    ControllerValue ctrl_value = self->get_controller_value(chn_id, ctrl_id);
    lua_pushinteger(L, ctrl_value);
    return 1;
}

static int mid_get_controller_modified_ts (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    ControllerId ctrl_id = (int)luaL_checkinteger(L, 3);
    Timestamp ts = self->get_controller_modified_ts(chn_id, ctrl_id);
    lua_pushinteger(L, ts);
    return 1;
}

static int mid_get_key_value (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id = (int)luaL_checkinteger(L, 3);
    KeyValue value = self->get_key_value(chn_id, key_id);
    lua_pushinteger(L, value);
    return 1;
}

static int mid_get_key_modified_ts (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id = (int)luaL_checkinteger(L, 3);
    Timestamp ts = self->get_key_modified_ts(chn_id, key_id);
    lua_pushinteger(L, ts);
    return 1;
}

static int mid_get_key_last_pressed_ts (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id = (int)luaL_checkinteger(L, 3);
    Timestamp ts = self->get_key_last_pressed_ts(chn_id, key_id);
    lua_pushinteger(L, ts);
    return 1;
}

static int mid_get_key_last_released_ts (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id = (int)luaL_checkinteger(L, 3);
    Timestamp ts = self->get_key_last_released_ts(chn_id, key_id);
    lua_pushinteger(L, ts);
    return 1;
}

static int mid_get_key_activation_counter (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id = (int)luaL_checkinteger(L, 3);
    unsigned int counter = self->get_key_activation_counter(chn_id, key_id);
    lua_pushinteger(L, counter);
    return 1;
}

static int mid_get_pitch_bend_value (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    HighResolutionValue value = self->get_pitch_bend_value(chn_id);
    lua_pushinteger(L, value);
    return 1;
}

static int mid_get_pitch_bend_modified_ts (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = self->get_pitch_bend_modified_ts(chn_id);
    lua_pushinteger(L, ts);
    return 1;
}

static int mid_get_high_resolution_controller_value (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    HighResolutionControllerId hi_ctrl_id = (int)luaL_checkinteger(L, 3);
    HighResolutionValue hi_ctrl_value = self->get_high_resolution_controller_value(chn_id, hi_ctrl_id);
    lua_pushinteger(L, hi_ctrl_value);
    return 1;
}

static int mid_get_high_resolution_controller_modified_ts (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    HighResolutionControllerId hi_ctrl_id = (int)luaL_checkinteger(L, 3);
    Timestamp ts = self->get_high_resolution_controller_modified_ts(chn_id, hi_ctrl_id);
    lua_pushinteger(L, ts);
    return 1;
}

static int mid_get_transport_value (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    TransportValue value = self->get_transport_value();
    switch(value)
    {
        case TransportValue::STARTED:
            lua_pushstring(L, "STARTED");
            break;
        
        case TransportValue::STOPPED:
            lua_pushstring(L, "STOPPED");
            break;
        
        case TransportValue::UNDEFINED:
            lua_pushstring(L, "UNDEFINED");
            break;
    }
    return 1;
}

static int mid_get_transport_value_modified_ts (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    Timestamp ts = self->get_transport_value_modified_ts();
    lua_pushinteger(L, ts);
    return 1;
}

static int mid_list_modified_controllers_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);

    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    ControllerId ctrl_id_min = luaL_optinteger(L, 4, RANGE_CONTROLLER_ID_MIN);
    ControllerId ctrl_id_max = luaL_optinteger(L, 5, RANGE_CONTROLLER_ID_MAX);

    std::vector<ControllerId> result = self->list_modified_controllers_after(
        chn_id, 
        ts, 
        ctrl_id_min,
        ctrl_id_max
    );

    push_uc_vector(L, result);
    return 1;
}

static int mid_list_modified_keys_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    KeyId key_id_min = luaL_optinteger(L, 4, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 5, RANGE_KEY_ID_MAX);

    std::vector<KeyId> result = self->list_modified_keys_after(
        chn_id, 
        ts,
        key_id_min,
        key_id_max
    );

    push_uc_vector(L, result);
    return 1;
}

static int mid_list_activated_keys_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    KeyId key_id_min = luaL_optinteger(L, 4, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 5, RANGE_KEY_ID_MAX);

    std::vector<KeyId> result = self->list_activated_keys_after(
        chn_id, 
        ts,
        key_id_min,
        key_id_max
    );

    push_uc_vector(L, result);
    return 1;
}

static int mid_list_released_keys_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    KeyId key_id_min = luaL_optinteger(L, 4, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 5, RANGE_KEY_ID_MAX);

    std::vector<KeyId> result = self->list_released_keys_after(
        chn_id, 
        ts,
        key_id_min,
        key_id_max
    );

    push_uc_vector(L, result);
    return 1;
}

static int mid_list_activated_keys_now (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id_min = luaL_optinteger(L, 3, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 4, RANGE_KEY_ID_MAX);

    std::vector<KeyId> result = self->list_activated_keys_now(
        chn_id, 
        key_id_min,
        key_id_max
    );

    push_uc_vector(L, result);
    return 1;
}

static int mid_last_activated_key (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id_min = luaL_optinteger(L, 3, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 4, RANGE_KEY_ID_MAX);

    std::optional<KeyId> result = self->last_activated_key(
        chn_id, 
        key_id_min,
        key_id_max
    );

    if(result.has_value()) {
        lua_pushinteger(L, result.value());
    } else {
        lua_pushnil(L);
    }

    return 1;
}

static int mid_any_key_changes_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);

    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    KeyId key_id_min = luaL_optinteger(L, 4, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 5, RANGE_KEY_ID_MAX);

    bool result = self->any_key_changes_after(
        chn_id, 
        ts,
        key_id_min,
        key_id_max
    );

    lua_pushboolean(L, result);
    return 1;
}

static int mid_any_key_activated_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    KeyId key_id_min = luaL_optinteger(L, 4, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 5, RANGE_KEY_ID_MAX);

    bool result = self->any_key_activated_after(
        chn_id, 
        ts,
        key_id_min,
        key_id_max
    );
    
    lua_pushboolean(L, result);
    return 1;
}

static int mid_any_key_released_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    KeyId key_id_min = luaL_optinteger(L, 4, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 5, RANGE_KEY_ID_MAX);

    bool result = self->any_key_released_after(
        chn_id, 
        ts,
        key_id_min,
        key_id_max
    );

    lua_pushboolean(L, result);
    return 1;
}

static int mid_any_controller_changes_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    ControllerId ctrl_id_min = luaL_optinteger(L, 4, RANGE_CONTROLLER_ID_MIN);
    ControllerId ctrl_id_max = luaL_optinteger(L, 5, RANGE_CONTROLLER_ID_MAX);

    bool result = self->any_controller_changes_after(
        chn_id, 
        ts,
        ctrl_id_min,
        ctrl_id_max
    );

    lua_pushboolean(L, result);
    return 1;
}

static int mid_any_changes_after (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    
    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    Timestamp ts = (int)luaL_checkinteger(L, 3);
    KeyId key_id_min = luaL_optinteger(L, 4, RANGE_KEY_ID_MIN);
    KeyId key_id_max = luaL_optinteger(L, 5, RANGE_KEY_ID_MAX);
    ControllerId ctrl_id_min = luaL_optinteger(L, 6, RANGE_CONTROLLER_ID_MIN);
    ControllerId ctrl_id_max = luaL_optinteger(L, 7, RANGE_CONTROLLER_ID_MAX);

    bool result = self->any_changes_after(
        chn_id, 
        ts,
        key_id_min,
        key_id_max,
        ctrl_id_min,
        ctrl_id_max
    );

    lua_pushboolean(L, result);
    return 1;
}

static int mid_is_started (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    bool result = self->is_started();
    lua_pushboolean(L, result);
    return 1;
}

static int mid_is_stopped (lua_State *L) {
    MidiInDevice* self = (MidiInDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_IN_DEVICE);
    bool result = self->is_stopped();
    lua_pushboolean(L, result);
    return 1;
}


static const luaL_Reg lumidi_native_midi_in_device_functions[] = {
    {"create",mid_create},
    {"get_timestamp", mid_get_timestamp},
    {NULL, NULL}
};

static const luaL_Reg lumidi_native_midi_in_device_methods[] = {
    {"open",mid_open},
    {"close",mid_close},
    {"get_controller_value",mid_get_controller_value},
    {"get_controller_modified_ts",mid_get_controller_modified_ts},
    {"get_key_value",mid_get_key_value},
    {"get_key_modified_ts",mid_get_key_modified_ts},
    {"get_key_last_pressed_ts",mid_get_key_last_pressed_ts},
    {"get_key_last_released_ts",mid_get_key_last_released_ts},
    {"get_key_activation_counter", mid_get_key_activation_counter},
    {"get_pitch_bend_value", mid_get_pitch_bend_value},
    {"get_pitch_bend_modified_ts", mid_get_pitch_bend_modified_ts},
    {"get_high_resolution_controller_value",mid_get_high_resolution_controller_value},
    {"get_high_resolution_controller_modified_ts",mid_get_high_resolution_controller_modified_ts},
    {"get_transport_value", mid_get_transport_value},
    {"get_transport_value_modified_ts", mid_get_transport_value_modified_ts},
    {"list_modified_controllers_after",mid_list_modified_controllers_after},
    {"list_modified_keys_after",mid_list_modified_keys_after},
    {"list_activated_keys_after",mid_list_activated_keys_after},
    {"list_released_keys_after",mid_list_released_keys_after},
    {"list_activated_keys_now",mid_list_activated_keys_now},
    {"last_activated_key",mid_last_activated_key},
    {"any_key_changes_after",mid_any_key_changes_after}, 
    {"any_key_activated_after",mid_any_key_activated_after},
    {"any_key_released_after",mid_any_key_released_after},
    {"any_controller_changes_after",mid_any_controller_changes_after},
    {"any_changes_after",mid_any_changes_after},
    {"is_started", mid_is_started},
    {"is_stopped", mid_is_stopped},
    {NULL, NULL}
};

extern "C" int luaopen_lumidi_native_midi_in_device (lua_State *L) {
    // Create metatable for MidiInDevice class
    luaL_newmetatable(L, METATABLE_MIDI_IN_DEVICE);

    // Redirect missing method search directly to the metatable
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");

    // Add GC support
    lua_pushcfunction(L, mid_gc);
    lua_setfield(L, -2, "__gc");

    // Add methods to the metatable
    luaL_setfuncs(L, lumidi_native_midi_in_device_methods, 0);

    luaL_newlib(L, lumidi_native_midi_in_device_functions);
    return 1;
}