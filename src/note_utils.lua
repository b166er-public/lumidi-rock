local EXPORTS = {};

local note_class_map = {
    ["C"] = 0,
    ["C#"] = 1,
    ["D"] = 2,
    ["D#"] = 3,
    ["E"] = 4,
    ["F"] = 5,
    ["F#"] = 6,
    ["G"] = 7,
    ["G#"] = 8,
    ["A"] = 9,
    ["A#"] = 10,
    ["B"] = 11
};

function EXPORTS.note_to_key(note_class, octave)
    if note_class_map[note_class] then
        return note_class_map[note_class] + (12*octave);
    else
        error("The note class " .. note_class .. " is unknown!");
    end
end

return EXPORTS;