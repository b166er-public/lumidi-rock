#include "midi-out-device.hpp"

#include <memory>

#include <lua.hpp>

using namespace lumidi::devices;

static const char* METATABLE_MIDI_OUT_DEVICE = "lumidi::devices::MidiOutDevice";

static int mod_create (lua_State *L) {
    int port_id = (int)luaL_checkinteger(L, 1);
  
    void* midi_out_device_raw_memory = lua_newuserdata(L, sizeof(MidiOutDevice));
    MidiOutDevice* self = new (midi_out_device_raw_memory) MidiOutDevice(port_id);

    luaL_getmetatable(L, METATABLE_MIDI_OUT_DEVICE);
    lua_setmetatable(L, -2);

    return 1;
}

static int mod_gc (lua_State *L) {
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);
    self->~MidiOutDevice();
    return 0;
}

static int mod_open (lua_State *L) {
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);
    if(!self->open()) {
        luaL_error(L, "Could not open MidiOutDevice on port %d", self->get_port_id());
    }
    return 0;
}

static int mod_close (lua_State *L) {
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);
    if(!self->close()) {
        luaL_error(L, "Could not close MidiOutDevice on port %d", self->get_port_id());
    }
    return 0;
}

static int mod_set_key_value (lua_State *L) {
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);

    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    KeyId key_id = (int)luaL_checkinteger(L, 3);
    KeyValue key_value = (int)luaL_checkinteger(L, 4);
    

    self->set_key_value(
        chn_id, 
        key_id, 
        key_value
    );
    return 0;
}

static int mod_set_controller_value (lua_State *L) {
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);

    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    ControllerId ctrl_id = (int)luaL_checkinteger(L, 3);
    ControllerValue ctrl_value = (int)luaL_checkinteger(L, 4);
    

    self->set_controller_value(
        chn_id, 
        ctrl_id, 
        ctrl_value
    );
    return 0;
}

static int mod_set_pitch_bend_value (lua_State *L) {
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);

    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    HighResolutionValue pb_value = (int)luaL_checkinteger(L, 3);

    self->set_pitch_bend_value(
        chn_id, 
        pb_value
    );
    return 0;
}

static int mod_set_high_resolution_controller_value (lua_State *L) {
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);

    ChannelId chn_id = (int)luaL_checkinteger(L, 2);
    HighResolutionControllerId hi_ctrl_id = (int)luaL_checkinteger(L, 3);
    HighResolutionValue hi_ctrl_value = (int)luaL_checkinteger(L, 4);
    

    self->set_high_resolution_controller_value(
        chn_id, 
        hi_ctrl_id, 
        hi_ctrl_value
    );
    return 0;
}

static int mod_send_clock(lua_State *L)
{
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);
    self->send_clock();
    return 0;
}

static int mod_send_start(lua_State *L)
{
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);
    self->send_start();
    return 0;
}

static int mod_send_stop(lua_State *L)
{
    MidiOutDevice* self = (MidiOutDevice*)luaL_checkudata(L, 1, METATABLE_MIDI_OUT_DEVICE);
    self->send_stop();
    return 0;
}

static const luaL_Reg lumidi_native_midi_out_device_functions[] = {
    {"create",mod_create},
    {NULL, NULL}
};

static const luaL_Reg lumidi_native_midi_out_device_methods[] = {
    {"open",mod_open},
    {"close",mod_close},
    {"set_key_value",mod_set_key_value},
    {"set_controller_value",mod_set_controller_value},
    {"set_pitch_bend_value",mod_set_pitch_bend_value},
    {"set_high_resolution_controller_value", mod_set_high_resolution_controller_value},
    {"send_clock", mod_send_clock},
    {"send_start", mod_send_start},
    {"send_stop", mod_send_stop},
    {NULL, NULL}
};

extern "C" int luaopen_lumidi_native_midi_out_device (lua_State *L) {
    // Create metatable for MidiOutDevice class
    luaL_newmetatable(L, METATABLE_MIDI_OUT_DEVICE);

    // Redirect missing method search directly to the metatable
    lua_pushvalue(L, -1);
    lua_setfield(L, -2, "__index");

    // Add GC support
    lua_pushcfunction(L, mod_gc);
    lua_setfield(L, -2, "__gc");

    // Add methods to the metatable
    luaL_setfuncs(L, lumidi_native_midi_out_device_methods, 0);

    luaL_newlib(L, lumidi_native_midi_out_device_functions);
    return 1;
}