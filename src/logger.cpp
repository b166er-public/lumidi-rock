#include <iostream>

#include "logger.hpp"


namespace lumidi {
namespace utils {

long Logger::delta_time(void)
{
    std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(now - this->start_ts).count();
}

Logger::Logger(std::string name)
{
    this->name = name;
    this->start_ts = std::chrono::high_resolution_clock::now();
}

void Logger::info(std::string msg)
{
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    std::cout 
        << "[ " << (this->name) << " | " << (this->delta_time()) << " | I ] : " 
        << msg 
        << std::endl;
}

void Logger::warning(std::string msg)
{
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    std::cout 
        << "[ " << (this->name) << " | " << (this->delta_time()) << " | W ] : " 
        << msg 
        << std::endl;
}

void Logger::error(std::string msg)
{
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    std::cout 
        << "[ " << (this->name) << " | " << (this->delta_time()) << " | E ] : " 
        << msg 
        << std::endl;
}

}
}