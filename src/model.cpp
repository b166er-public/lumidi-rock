#include "model.hpp"

#include <algorithm>  
#include <ostream>
#include <iostream>

namespace lumidi {
namespace model {

bool ControllerState::set(Timestamp ts, ControllerValue value) 
{
    if(this->value != value) {
        this->modified = std::max(this->modified, ts);
        this->value = value;
        return true;
    } else {
        return false;
    }
}

bool KeyState::set(Timestamp ts, KeyValue value) 
{
    if(this->value != value) {
        this->modified = std::max(this->modified, ts);
        bool is_now_pressed = (this->value == 0) && (value > 0);
        bool is_now_released = (this->value > 0) && (value == 0);
        if(is_now_pressed) { this->last_pressed = this->modified; this->activation_counter++; }
        if(is_now_released) { this->last_released = this->modified; }
        this->value = value;
        return true;
    } else {
        return false;
    }
}

bool 
PitchBend::set(
    Timestamp ts, 
    HighResolutionValue value
)
{
    if(this->value != value) {
        this->modified = std::max(this->modified, ts);
        this->value = value;
        return true;
    } else {
        return false;
    }
}

bool 
TransportState::set(
    Timestamp ts,
    TransportValue value
)
{
    if(this->value != value) {
        this->modified = std::max(this->modified, ts);
        this->value = value;
        return true;
    } else {
        return false;
    }
}

bool 
DeviceState::set_key_value(
    Timestamp ts, 
    ChannelId chn_id, 
    KeyId key_id, 
    KeyValue key_value
)
{
    return this->channels[chn_id].keys[key_id].set(ts, key_value);
}

bool 
DeviceState::set_controller_value(
    Timestamp ts, 
    ChannelId chn_id, 
    ControllerId ctrl_id, 
    ControllerValue ctrl_value
)
{
    return this->channels[chn_id].controllers[ctrl_id].set(ts, ctrl_value);
}

bool 
DeviceState::set_pitch_bend_value(
    Timestamp ts, 
    ChannelId chn_id, 
    HighResolutionValue value
)
{
    return this->channels[chn_id].pitch_bend.set(ts, value);
}

std::pair<bool, bool> 
DeviceState::set_high_resolution_controller_value(
    Timestamp ts, 
    ChannelId chn_id, 
    HighResolutionControllerId hi_ctrl_id, 
    HighResolutionValue hi_ctrl_value
)
{
    ChannelState& chn_state = this->channels[chn_id % NUMBER_OF_CHANNELS];
    hi_ctrl_id = hi_ctrl_id % NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL;
    ControllerState& msb_state = chn_state.controllers[hi_ctrl_id];
    ControllerState& lsb_state = chn_state.controllers[NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL + hi_ctrl_id];
    ControllerValue msb_new_value = (hi_ctrl_value >> 7) & 0x7F;
    ControllerValue lsb_new_value = (hi_ctrl_value) & 0x7F;
    bool changed_msb = msb_state.set(ts, msb_new_value);
    bool changed_lsb = lsb_state.set(ts, lsb_new_value);
    return std::pair<bool, bool>{changed_msb, changed_lsb};
}

bool 
DeviceState::set_transport_value(
    Timestamp ts, 
    TransportValue value
)
{
    return this->transport.set(ts, value);
}

ControllerValue 
DeviceState::get_controller_value(
    ChannelId chn_id, 
    ControllerId ctrl_id
)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].controllers[ctrl_id % NUMBER_OF_CONTROLLERS_PER_CHANNEL].value;
}

Timestamp 
DeviceState::get_controller_modified_ts(
    ChannelId chn_id, 
    ControllerId ctrl_id
)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].controllers[ctrl_id % NUMBER_OF_CONTROLLERS_PER_CHANNEL].modified;
}

KeyValue 
DeviceState::get_key_value(
    ChannelId chn_id, 
    KeyId key_id
)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].keys[key_id % NUMBER_OF_KEYS_PER_CHANNEL].value;
}

Timestamp 
DeviceState::get_key_modified_ts(
    ChannelId chn_id, 
    KeyId key_id
)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].keys[key_id % NUMBER_OF_KEYS_PER_CHANNEL].modified;
}

Timestamp 
DeviceState::get_key_last_pressed_ts(
    ChannelId chn_id, 
    KeyId key_id
)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].keys[key_id % NUMBER_OF_KEYS_PER_CHANNEL].last_pressed;
}

Timestamp 
DeviceState::get_key_last_released_ts(
    ChannelId chn_id, 
    KeyId key_id
)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].keys[key_id % NUMBER_OF_KEYS_PER_CHANNEL].last_released;
}

unsigned int 
DeviceState::get_key_activation_counter(
    ChannelId chn_id, 
    KeyId key_id
)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].keys[key_id % NUMBER_OF_KEYS_PER_CHANNEL].activation_counter;
}

HighResolutionValue 
DeviceState::get_pitch_bend_value(ChannelId chn_id)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].pitch_bend.value;
}

Timestamp 
DeviceState::get_pitch_bend_modified_ts(ChannelId chn_id)
{
    return this->channels[chn_id % NUMBER_OF_CHANNELS].pitch_bend.modified;
}

HighResolutionValue 
DeviceState::get_high_resolution_controller_value(
    ChannelId chn_id, 
    HighResolutionControllerId hi_ctrl_id
)
{
    ChannelState& chn_state = this->channels[chn_id % NUMBER_OF_CHANNELS];
    hi_ctrl_id = hi_ctrl_id % NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL;
    ControllerState& msb_state = chn_state.controllers[hi_ctrl_id];
    ControllerState& lsb_state = chn_state.controllers[NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL + hi_ctrl_id];
    HighResolutionValue result = ((msb_state.value & 0x7F) << 7) | (lsb_state.value & 0x7F);
    return result;
}

Timestamp 
DeviceState::get_high_resolution_controller_modified_ts(
    ChannelId chn_id, 
    HighResolutionControllerId hi_ctrl_id
)
{
    ChannelState& chn_state = this->channels[chn_id % NUMBER_OF_CHANNELS];
    hi_ctrl_id = hi_ctrl_id % NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL;
    ControllerState& msb_state = chn_state.controllers[hi_ctrl_id];
    ControllerState& lsb_state = chn_state.controllers[NUMBER_OF_HIGH_RESOLUTION_CONTROLLERS_PER_CHANNEL + hi_ctrl_id];
    return std::max<Timestamp>(msb_state.modified, lsb_state.modified);
}

TransportValue 
DeviceState::get_transport_value(void)
{
    return this->transport.value;
}

Timestamp 
DeviceState::get_transport_value_modified_ts(void)
{
    return this->transport.modified;
}

std::vector<ControllerId> 
DeviceState::list_modified_controllers_after(
    ChannelId chn_id, 
    Timestamp ts, 
    ControllerId ctrl_id_min,
    ControllerId ctrl_id_max
)
{   
    std::vector<ControllerId> result;
    ControllerState* ctrl_states = this->channels[chn_id % NUMBER_OF_CHANNELS].controllers;
    ControllerId ctrl_id_min_safe = std::max(RANGE_CONTROLLER_ID_MIN, ctrl_id_min);
    ControllerId ctrl_id_max_safe = std::min(RANGE_CONTROLLER_ID_MAX, ctrl_id_max);

    for(unsigned char cid = ctrl_id_min_safe; cid <= ctrl_id_max_safe; cid++)
    {
        if(ctrl_states[cid].modified > ts) {
            result.push_back(cid);
        }
    }

    return result;
}

std::vector<KeyId> 
DeviceState::list_modified_keys_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{   
    std::vector<KeyId> result;
    KeyState* key_states = this->channels[chn_id % NUMBER_OF_CHANNELS].keys;
    KeyId key_id_min_safe = std::max(RANGE_KEY_ID_MIN, key_id_min);
    KeyId key_id_max_safe = std::min(RANGE_KEY_ID_MAX, key_id_max);

    for(unsigned char kid = key_id_min_safe; kid <= key_id_max_safe; kid++)
    {
        if(key_states[kid].modified > ts) {
            result.push_back(kid);
        }
    }

    return result;

}

std::vector<KeyId> 
DeviceState::list_activated_keys_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{   
    std::vector<KeyId> result;
    KeyState* key_states = this->channels[chn_id % NUMBER_OF_CHANNELS].keys;
    KeyId key_id_min_safe = std::max(RANGE_KEY_ID_MIN, key_id_min);
    KeyId key_id_max_safe = std::min(RANGE_KEY_ID_MAX, key_id_max);

    for(unsigned char kid = key_id_min_safe; kid <= key_id_max_safe; kid++)
    {
        if( key_states[kid].last_pressed > ts ) {
            result.push_back(kid);
        }
    }

    return result;
}

std::vector<KeyId> 
DeviceState::list_released_keys_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{   
    std::vector<KeyId> result;
    KeyState* key_states = this->channels[chn_id % NUMBER_OF_CHANNELS].keys;
    KeyId key_id_min_safe = std::max(RANGE_KEY_ID_MIN, key_id_min);
    KeyId key_id_max_safe = std::min(RANGE_KEY_ID_MAX, key_id_max);

    for(unsigned char kid = key_id_min_safe; kid <= key_id_max_safe; kid++)
    {
        if( key_states[kid].last_released > ts) {
            result.push_back(kid);
        }
    }

    return result;
}

std::vector<KeyId> 
DeviceState::list_activated_keys_now(
    ChannelId chn_id,
    KeyId key_id_min,
    KeyId key_id_max
)
{   
    std::vector<KeyId> result;
    KeyState* key_states = this->channels[chn_id % NUMBER_OF_CHANNELS].keys;
    KeyId key_id_min_safe = std::max(RANGE_KEY_ID_MIN, key_id_min);
    KeyId key_id_max_safe = std::min(RANGE_KEY_ID_MAX, key_id_max);

    for(unsigned char kid = key_id_min_safe; kid <= key_id_max_safe; kid++)
    {
        if( key_states[kid].value > 0) {
            result.push_back(kid);
        }
    }

    return result;
}

std::optional<KeyId> 
DeviceState::last_activated_key(
    ChannelId chn_id,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    std::vector<KeyId> activated_keys = this->list_activated_keys_now(
        chn_id, 
        key_id_min, 
        key_id_max
    );

    if(activated_keys.empty()) {
        return std::optional<KeyId>{std::nullopt};
    } else {
        KeyId last_key_id = activated_keys[0];
        Timestamp last_key_modified_ts = this->get_key_modified_ts(chn_id,last_key_id);

        for(KeyId c_kid : activated_keys) {
            Timestamp c_modified_ts = this->get_key_modified_ts(chn_id,c_kid);
            if(c_modified_ts > last_key_modified_ts) {
                last_key_id = c_kid;
                last_key_modified_ts = c_modified_ts;
            }
        }
        return last_key_id;
    }
}

bool 
DeviceState::any_key_changes_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    KeyState* key_states = this->channels[chn_id % NUMBER_OF_CHANNELS].keys;
    KeyId key_id_min_safe = std::max(RANGE_KEY_ID_MIN, key_id_min);
    KeyId key_id_max_safe = std::min(RANGE_KEY_ID_MAX, key_id_max);

    for(unsigned char kid = key_id_min_safe; kid <= key_id_max_safe; kid++)
    {
        if( key_states[kid].modified > ts ) {
            return true;
        }
    }

    return false;
}

bool 
DeviceState::any_key_activated_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    KeyState* key_states = this->channels[chn_id % NUMBER_OF_CHANNELS].keys;
    KeyId key_id_min_safe = std::max(RANGE_KEY_ID_MIN, key_id_min);
    KeyId key_id_max_safe = std::min(RANGE_KEY_ID_MAX, key_id_max);

    for(unsigned char kid = key_id_min_safe; kid <= key_id_max_safe; kid++)
    {
        if( key_states[kid].last_pressed > ts ) {
            return true;
        }
    }

    return false;
}

bool 
DeviceState::any_key_released_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    KeyState* key_states = this->channels[chn_id % NUMBER_OF_CHANNELS].keys;
    KeyId key_id_min_safe = std::max(RANGE_KEY_ID_MIN, key_id_min);
    KeyId key_id_max_safe = std::min(RANGE_KEY_ID_MAX, key_id_max);

    for(unsigned char kid = key_id_min_safe; kid <= key_id_max_safe; kid++)
    {
        if( key_states[kid].last_released > ts ) {
            return true;
        }
    }

    return false;
}

bool 
DeviceState::any_controller_changes_after(
    ChannelId chn_id, 
    Timestamp ts,
    ControllerId ctrl_id_min,
    ControllerId ctrl_id_max
)
{
    ControllerState* ctrl_states = this->channels[chn_id % NUMBER_OF_CHANNELS].controllers;
    ControllerId ctrl_id_min_safe = std::max(RANGE_CONTROLLER_ID_MIN, ctrl_id_min);
    ControllerId ctrl_id_max_safe = std::min(RANGE_CONTROLLER_ID_MAX, ctrl_id_max);

    for(unsigned char cid = ctrl_id_min_safe; cid <= ctrl_id_max_safe; cid++)
    {
        if( ctrl_states[cid].modified > ts ) {
            return true;
        }
    }

    return false;
}

bool
DeviceState::any_pitch_bend_changes_after(
    ChannelId chn_id, 
    Timestamp ts
)
{
    return (this->channels[chn_id % NUMBER_OF_CHANNELS].pitch_bend.modified > ts);
}

bool 
DeviceState::any_changes_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max,
    ControllerId ctrl_id_min,
    ControllerId ctrl_id_max
)
{
    bool any_controller_changes_after = 
        this->any_controller_changes_after(
            chn_id, 
            ts,
            ctrl_id_min,
            ctrl_id_max
        );

    bool any_key_changes_after = 
        this->any_key_changes_after(
            chn_id, 
            ts,
            key_id_min,
            key_id_max
        );

    bool any_pitch_bend_changes_after =
        this->any_pitch_bend_changes_after(
            chn_id, 
            ts
        );

    return any_controller_changes_after || any_key_changes_after || any_pitch_bend_changes_after;
}

bool 
DeviceState::is_started()
{
    return this->transport.value == TransportValue::STARTED;
}

bool 
DeviceState::is_stopped()
{
    return this->transport.value == TransportValue::STOPPED;
}

}
}