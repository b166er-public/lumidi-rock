#include "midi-in-device.hpp"

#include "info.hpp"

#include <sstream> 
#include <algorithm>

namespace lumidi {
namespace devices {

void MidiInDevice::rt_midi_in_receiver_callback(
    double timeStamp, 
    std::vector<unsigned char> *message, 
    void *user_data
)
{
    if(user_data) {
        MidiInDevice* self = (MidiInDevice*) user_data;
        if(self->is_opened) {
            // Derive type
            unsigned char status_byte = (*message)[0];
            unsigned char msg_type = status_byte & 0xF0;
            unsigned char msg_chn = status_byte & 0x0F;

            if(msg_type == 0x80) /* Note Off */ {
                const std::lock_guard<std::mutex> io_lock(self->state_access_mutex);
                unsigned char key_id = (*message)[1];
                self->state.set_key_value(
                    lumidi::info::get_timestamp(),
                    msg_chn,
                    key_id,
                    0
                );
            } else if( (msg_type == 0x90) || (msg_type == 0xA0) ) /* Note On */ {
                const std::lock_guard<std::mutex> io_lock(self->state_access_mutex);
                unsigned char key_id = (*message)[1];
                unsigned char velocity = (*message)[2];
                self->state.set_key_value(
                    lumidi::info::get_timestamp(),
                    msg_chn,
                    key_id,
                    velocity
                );
            } else if(msg_type == 0xB0) /* Control change */ {
                const std::lock_guard<std::mutex> io_lock(self->state_access_mutex);
                unsigned char ctrl_id = (*message)[1];
                unsigned char value = (*message)[2];
                self->state.set_controller_value(
                    lumidi::info::get_timestamp(),
                    msg_chn,
                    ctrl_id,
                    value
                );
            } else if(msg_type == 0xE0) /* Pitch bend changes */ {
                unsigned char lsb = (*message)[1];
                unsigned char msb = (*message)[2];
                unsigned int value = (((unsigned int)msb) << 7) | ((unsigned int)lsb);
                self->state.set_pitch_bend_value(
                    lumidi::info::get_timestamp(),
                    msg_chn,
                    value
                );
            } else if(msg_type == 0xFA) /* Start */ {
                self->state.set_transport_value(
                    lumidi::info::get_timestamp(),
                    TransportValue::STARTED
                );
            } else if(msg_type == 0xFC) /* Stop */ {
                self->state.set_transport_value(
                    lumidi::info::get_timestamp(),
                    TransportValue::STOPPED
                );
            } else {
                std::cout << "Unknown MSG-TYPE " << ((int)msg_type) << std::endl;
            }
        }
    }
}

MidiInDevice::MidiInDevice(unsigned int port_id) 
{
    this->port_id = port_id;

    std::stringstream logger_name_stream;
    logger_name_stream << "MidiInDevice";
    logger_name_stream << " [ " << (this->port_id) << " ]";

    this->logger = std::unique_ptr<lumidi::utils::Logger>(
        new lumidi::utils::Logger(
            logger_name_stream.str()
        )
    );
    this->rt_port_handler = nullptr;
    this->is_opened = false;
}

MidiInDevice::~MidiInDevice()
{
    if(this->rt_port_handler) {
        try {
            this->rt_port_handler->closePort();
        }
        catch ( RtMidiError &error ) {
            this->logger->error("Could not destroy device!");
            error.printMessage();
        }
        this->rt_port_handler.reset();
    }
}

unsigned int MidiInDevice::get_port_id(void)
{
    return this->port_id;
}

bool MidiInDevice::open(void)
{
    if(this->is_opened) {
        this->logger->error("Can not open already opened device!");
    } else {
        try {
            this->rt_port_handler = std::unique_ptr<RtMidiIn>(new RtMidiIn());
            this->rt_port_handler->openPort(this->port_id);
            this->rt_port_handler->setCallback(
                &rt_midi_in_receiver_callback,
                (void*) this
            );
            this->rt_port_handler->ignoreTypes(true,true,true);
            this->is_opened = true; return true;
        }
        catch ( RtMidiError &error ) {
            this->logger->error("Could not open MidiInDevice!");
            error.printMessage();
            this->rt_port_handler.reset();
        }
    }
    return false;
}

bool MidiInDevice::close(void)
{
    if(this->is_opened) {
        try {
            this->rt_port_handler->cancelCallback();
            this->rt_port_handler->closePort();
            this->rt_port_handler.reset();
            this->is_opened = false; return true;
        }
        catch ( RtMidiError &error ) {
            this->logger->error("Could not close MidiInDevice!");
            error.printMessage();
        }
    } else {
        this->logger->error("Can not closed a non opened device!");
    }

    return false;
}

ControllerValue 
MidiInDevice::get_controller_value(
    ChannelId chn_id, 
    ControllerId ctrl_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_controller_value(chn_id, ctrl_id);
}

Timestamp 
MidiInDevice::get_controller_modified_ts(
    ChannelId chn_id, 
    ControllerId ctrl_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_controller_modified_ts(chn_id, ctrl_id);
}

KeyValue 
MidiInDevice::get_key_value(
    ChannelId chn_id, 
    KeyId key_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_key_value(chn_id, key_id);
}

Timestamp 
MidiInDevice::get_key_modified_ts(
    ChannelId chn_id, 
    KeyId key_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_key_modified_ts(chn_id, key_id);
}

Timestamp 
MidiInDevice::get_key_last_pressed_ts(
    ChannelId chn_id, 
    KeyId key_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_key_last_pressed_ts(chn_id, key_id);
}

Timestamp 
MidiInDevice::get_key_last_released_ts(
    ChannelId chn_id, 
    KeyId key_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_key_last_released_ts(chn_id, key_id);
}

unsigned int 
MidiInDevice::get_key_activation_counter(
    ChannelId chn_id, 
    KeyId key_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_key_activation_counter(chn_id, key_id);
}

HighResolutionValue 
MidiInDevice::get_pitch_bend_value(ChannelId chn_id)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_pitch_bend_value(chn_id);
}

Timestamp 
MidiInDevice::get_pitch_bend_modified_ts(ChannelId chn_id)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_pitch_bend_modified_ts(chn_id);
}

HighResolutionValue 
MidiInDevice::get_high_resolution_controller_value(
    ChannelId chn_id, 
    HighResolutionControllerId hi_ctrl_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_high_resolution_controller_value(chn_id, hi_ctrl_id);
}

Timestamp 
MidiInDevice::get_high_resolution_controller_modified_ts(
    ChannelId chn_id, 
    HighResolutionControllerId hi_ctrl_id
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_high_resolution_controller_modified_ts(chn_id, hi_ctrl_id);
}

TransportValue 
MidiInDevice::get_transport_value(void)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_transport_value();
}

Timestamp 
MidiInDevice::get_transport_value_modified_ts(void)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.get_transport_value_modified_ts();
}

std::vector<ControllerId> 
MidiInDevice::list_modified_controllers_after(
    ChannelId chn_id, 
    Timestamp ts, 
    ControllerId ctrl_id_min,
    ControllerId ctrl_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.list_modified_controllers_after(chn_id, ts, ctrl_id_min, ctrl_id_max);
}

std::vector<KeyId> 
MidiInDevice::list_modified_keys_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.list_modified_keys_after(chn_id, ts, key_id_min, key_id_max);
}

std::vector<KeyId> 
MidiInDevice::list_activated_keys_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.list_activated_keys_after(chn_id, ts, key_id_min, key_id_max);
}

std::vector<KeyId> 
MidiInDevice::list_released_keys_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.list_released_keys_after(chn_id, ts, key_id_min, key_id_max);
}

std::vector<KeyId> 
MidiInDevice::list_activated_keys_now(
    ChannelId chn_id,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.list_activated_keys_now(chn_id, key_id_min, key_id_max);
}

std::optional<KeyId> 
MidiInDevice::last_activated_key(
    ChannelId chn_id,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.last_activated_key(chn_id, key_id_min, key_id_max);
}

bool 
MidiInDevice::any_key_changes_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.any_changes_after(chn_id, ts, key_id_min, key_id_max);
}

bool 
MidiInDevice::any_key_activated_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.any_key_activated_after(chn_id, ts, key_id_min, key_id_max);
}

bool 
MidiInDevice::any_key_released_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.any_key_released_after(chn_id, ts, key_id_min, key_id_max);
}

bool 
MidiInDevice::any_controller_changes_after(
    ChannelId chn_id, 
    Timestamp ts,
    ControllerId ctrl_id_min,
    ControllerId ctrl_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.any_controller_changes_after(chn_id, ts, ctrl_id_min, ctrl_id_max);
}

bool 
MidiInDevice::any_changes_after(
    ChannelId chn_id, 
    Timestamp ts,
    KeyId key_id_min,
    KeyId key_id_max,
    ControllerId ctrl_id_min,
    ControllerId ctrl_id_max
)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.any_changes_after(chn_id, ts, key_id_min, key_id_max, ctrl_id_min, ctrl_id_max);
}

bool 
MidiInDevice::is_started(void)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.is_started();
}

bool 
MidiInDevice::is_stopped(void)
{
    const std::lock_guard<std::mutex> io_lock(this->state_access_mutex);
    return this->state.is_stopped();
}

int 
MidiInDevice::get_timestamp(void) 
{
    return (int) lumidi::info::get_timestamp();
}

}
}