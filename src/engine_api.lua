local EXPORTS = {};

local info = require("lumidi_native_info");
local engine = require("lumidi_engine");

local EngineApi = {
    __name = "EngineApi",
}

function EngineApi:new(engine_instance)
    local obj = {};

    self.__index = self;
    setmetatable(obj, self);

    obj.engine_instance = engine_instance;
    return obj;
end

function EngineApi:get_input_port(alias)
    return self.engine_instance.midi_dev_in[alias];
end

function EngineApi:get_output_port(alias)
    return self.engine_instance.midi_dev_out[alias];
end

function EngineApi:get_inputs()
    return self.engine_instance.midi_dev_in;
end

function EngineApi:get_outputs()
    return self.engine_instance.midi_dev_out;
end

function EngineApi:add_layer(fn_name, fn)
    table.insert(
        self.engine_instance.layer_context_fresh,
        {
            name = fn_name,
            cr = coroutine.create(fn),
            wait = 0,
            alive = true
        }
    );
end

function EngineApi:get_tick()
    return self.engine_instance.tick;
end

function EngineApi:sleep_beat_aligned(beats)
    local ticks_now = self:get_tick();
    local tpb = self:get_tpb();
    local ticks_unit_next_beat =  tpb - (ticks_now % tpb);
    local ticks_until_target = ticks_unit_next_beat + math.max(0, (beats-1) )*tpb;
    self:sleep(ticks_until_target);
end

function EngineApi:sleep_beat_grid_aligned(grid, offset)
    local ticks_now = self:get_tick();
    local tpb = self:get_tpb();
    local abs_beat = math.floor(ticks_now / tpb);
    local is_perfectly_on_beat = ( (ticks_now % tpb) == 0);
    if is_perfectly_on_beat then
        if ((abs_beat % grid) - offset) == 0 then
            return;
        else
            local beats_to_sleep = abs_beat % grid + offset;
            self:sleep_beat_aligned(beats_to_sleep-1);
        end
    else
        local beats_to_sleep = abs_beat % grid + offset;
        self:sleep_beat_aligned(beats_to_sleep-1);
    end
end

function EngineApi:get_bpm()
    return self.engine_instance.config.bpm;
end

function EngineApi:get_tpb()
    return self.engine_instance.config.tpb;
end

function EngineApi:sleep(ticks)
    coroutine.yield(ticks);
end

function EngineApi:sleep_beat(beats)
    coroutine.yield(
        math.floor(
            self:get_tpb() * beats
        )
    );
end

function EngineApi:get_timestamp()
    return self.engine_instance.get_timestamp();
end

function EngineApi:request_stop()
    self.engine_instance.termination_requested = true;
end

gate = nil;

function EngineApi:create_gate()
    if not gate then
        gate = require("lumidi_gate");
    end
    
    return gate.create(self);
end

sequence_layer = nil;

function EngineApi:create_sequence_layer(
    out_port_alias,
    channel,
    keys,
    durations,
    velocities,
    play_gate_auto_close
)
    if not gate then
        sequence_layer = require("lumidi_sequence_layer");
    end
    
    return sequence_layer.create(
        self,
        out_port_alias,
        channel,
        keys,
        durations,
        velocities,
        play_gate_auto_close
    );
end

function EngineApi:play_note(
    out_port_alias,
    channel,
    key,
    duration,
    velocity
)
    local out_port = self:get_output_port(out_port_alias);
    out_port:set_key_value(channel, key, velocity);
    self:sleep_beat(duration);
    out_port:set_key_value(channel, key, 0);
end


function EXPORTS.create(engine_instance)
    if engine.instanceof(engine_instance) then
        return EngineApi:new(engine_instance)
    else
        error("Please provide an instance of Engine as parameter!");
    end
end

function EXPORTS.instanceof(obj)
    return getmetatable(obj) == EngineApi;
end

return EXPORTS;