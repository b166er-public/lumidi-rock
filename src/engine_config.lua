local EXPORTS = {};

local EngineConfig = {
    __name = "EngineConfig",
    master_clock = false,
    bpm = 120,
    tpb = 2800,
    midi_dev_in = {},
    midi_dev_out = {},
    layers = {}
};

function EngineConfig:new()
    local obj = {};
    self.__index = self;
    setmetatable(obj, self);
    return obj;
end

function EngineConfig:set_bpm(value)
    self.bpm = value;
    return self;
end

function EngineConfig:set_tpb(value)
    self.tpb = value;
    return self;
end

function EngineConfig:enable_master_clock(value)
    self.master_clock = value;
    return self;
end

function EngineConfig:add_input_device(alias, port_id)
    self.midi_dev_in[alias] = port_id;
    return self;
end

function EngineConfig:add_output_device(alias, port_id)
    self.midi_dev_out[alias] = port_id;
    return self;
end

function EngineConfig:add_layer(alias,layer_function)
    table.insert(
        self.layers,
        {
            fn = layer_function,
            name = alias
        }
    );
    return self;
end



function EXPORTS.create()
    return EngineConfig:new();
end

function EXPORTS.instanceof(obj)
    return getmetatable(obj) == EngineConfig;
end

return EXPORTS;