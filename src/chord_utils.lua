local EXPORTS = {};

local chord_map = {
    ["Major"] = {0,4,7},
    ["7"] = {0,4,7,10},
    ["maj7"] = {0,4,7,11},
    ["dim7"] = {0,3,6,9},
    ["7#5"] = {0,4,8,10},
    ["7b9"] = {0,4,7,10,1},
    ["m6"] = {0,3,7,9},
    ["m"] = {0,3,7},
    ["m7"] = {0,3,7,10},
    ["dim"] = {0,3,6},
    ["7b5"] = {0,4,6,10},
    ["m7b5"] = {0,3,6,10},
    ["6"] = {0,4,7,9},
    ["6add9"] = {0,4,7,9,2}
};

function EXPORTS.chord_to_intervals(chord_name, offset)
    if chord_map[chord_name] then
        local result = {};
        offset = offset or 0;
        for _, note in ipairs( chord_map[chord_name] ) do
            table.insert(result, note + offset);
        end
        return result;
    else
        error("The chord " .. chord_name .. " is unknown!");
    end
end

return EXPORTS;