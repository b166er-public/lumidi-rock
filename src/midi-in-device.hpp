#pragma once

#include "logger.hpp"
#include "model.hpp"

#include <memory>
#include <atomic>
#include <mutex>

#include <rtmidi/RtMidi.h>

namespace lumidi {
namespace devices {

using namespace lumidi::model;

class MidiInDevice {

private:
    std::unique_ptr<lumidi::utils::Logger> logger;
    std::atomic<bool> is_opened;
    unsigned int port_id;
    std::unique_ptr<RtMidiIn> rt_port_handler;

    DeviceState state;
    std::mutex state_access_mutex;

    static void rt_midi_in_receiver_callback(
        double timeStamp, 
        std::vector<unsigned char> *message, 
        void *user_data
    );

public:
    MidiInDevice(unsigned int port_id);
    virtual ~MidiInDevice();

    unsigned int get_port_id(void);
    bool open(void);
    bool close(void);

    /* Query - Basic */
    ControllerValue get_controller_value(ChannelId chn_id, ControllerId ctrl_id);
    Timestamp get_controller_modified_ts(ChannelId chn_id, ControllerId ctrl_id);

    KeyValue get_key_value(ChannelId chn_id, KeyId key_id);
    Timestamp get_key_modified_ts(ChannelId chn_id, KeyId key_id);
    Timestamp get_key_last_pressed_ts(ChannelId chn_id, KeyId key_id);
    Timestamp get_key_last_released_ts(ChannelId chn_id, KeyId key_id);
    unsigned int get_key_activation_counter(ChannelId chn_id, KeyId key_id);

    HighResolutionValue get_pitch_bend_value(ChannelId chn_id);
    Timestamp get_pitch_bend_modified_ts(ChannelId chn_id);

    HighResolutionValue get_high_resolution_controller_value(ChannelId chn_id, HighResolutionControllerId hi_ctrl_id);
    Timestamp get_high_resolution_controller_modified_ts(ChannelId chn_id, HighResolutionControllerId hi_ctrl_id);

    TransportValue get_transport_value(void);
    Timestamp get_transport_value_modified_ts(void);

    /* Query - Advanced */
    std::vector<ControllerId> list_modified_controllers_after(
        ChannelId chn_id, 
        Timestamp ts, 
        ControllerId ctrl_id_min = RANGE_CONTROLLER_ID_MIN,
        ControllerId ctrl_id_max = RANGE_CONTROLLER_ID_MAX
    );
    
    std::vector<KeyId> list_modified_keys_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    std::vector<KeyId> list_activated_keys_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    std::vector<KeyId> list_released_keys_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );
    
    std::vector<KeyId> list_activated_keys_now(
        ChannelId chn_id,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    std::optional<KeyId> last_activated_key(
        ChannelId chn_id,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    bool any_key_changes_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );
    
    bool any_key_activated_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    bool any_key_released_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX
    );

    bool any_controller_changes_after(
        ChannelId chn_id, 
        Timestamp ts,
        ControllerId ctrl_id_min = RANGE_CONTROLLER_ID_MIN,
        ControllerId ctrl_id_max = RANGE_CONTROLLER_ID_MAX
    );

    bool any_changes_after(
        ChannelId chn_id, 
        Timestamp ts,
        KeyId key_id_min = RANGE_KEY_ID_MIN,
        KeyId key_id_max = RANGE_KEY_ID_MAX,
        ControllerId ctrl_id_min = RANGE_CONTROLLER_ID_MIN,
        ControllerId ctrl_id_max = RANGE_CONTROLLER_ID_MAX
    );

    bool is_started(void);
    bool is_stopped(void);

    static int get_timestamp(void);
};

}
}