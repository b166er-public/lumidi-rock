#pragma once

#include "logger.hpp"
#include "model.hpp"

#include <memory>

#include <rtmidi/RtMidi.h>

namespace lumidi {
namespace devices {

using namespace lumidi::model;

typedef void (*KeyChangedCallback) (
    Timestamp ts, 
    ChannelId chn_id, 
    KeyId key_id, 
    KeyValue key_value,
    void* user_data
);

typedef void (*ControllerChangedCallback) (
    Timestamp ts, 
    ChannelId chn_id, 
    ControllerId ctrl_id, 
    ControllerValue ctrl_value,
    void* user_data
);

typedef void (*PitchBendChangedCallback) (
    Timestamp ts, 
    ChannelId chn_id, 
    HighResolutionValue pb_value,
    void* user_data
);

class MidiOutDevice {

private:
    std::unique_ptr<lumidi::utils::Logger> logger;
    bool is_opened;
    unsigned int port_id;
    std::unique_ptr<RtMidiOut> rt_port_handler;

    DeviceState state;
    std::vector<KeyChangedCallback> key_callbacks;
    std::vector<ControllerChangedCallback> controller_callbacks;
    std::vector<PitchBendChangedCallback> pitch_bend_callbacks;
    

    static void default_key_cb(
        Timestamp ts, 
        ChannelId chn_id, 
        KeyId key_id, 
        KeyValue key_value,
        void* user_data
    );

    static void default_controller_cb(
        Timestamp ts, 
        ChannelId chn_id, 
        ControllerId ctrl_id, 
        ControllerValue ctrl_value,
        void* user_data
    );

    static void default_pitch_bend_cb(
        Timestamp ts, 
        ChannelId chn_id, 
        HighResolutionValue pb_value,
        void* user_data
    );

public:

    MidiOutDevice(unsigned int port_id);
    virtual ~MidiOutDevice();

    unsigned int get_port_id(void);
    bool open(void);
    bool close(void);

    /* Modify */
    void set_key_value(
        ChannelId chn_id, 
        KeyId key_id, 
        KeyValue key_value
    );
    
    void set_controller_value(
        ChannelId chn_id, 
        ControllerId ctrl_id, 
        ControllerValue ctrl_value
    );

    void set_pitch_bend_value(
        ChannelId chn_id, 
        HighResolutionValue pb_value
    );

    void set_high_resolution_controller_value(
        ChannelId chn_id, 
        HighResolutionControllerId hi_ctrl_id, 
        HighResolutionValue hi_ctrl_value
    );

    void send_clock(void);
    void send_start(void);
    void send_stop(void);

    /* Callbacks (triggered on real modification) */
    void add_key_callback(KeyChangedCallback callback);
    void add_controller_callback(ControllerChangedCallback callback);
    void add_pitch_bend_callback(PitchBendChangedCallback callback);
    
    void reset_key_callbacks(void);
    void reset_controller_callbacks(void);
    void reset_pitch_bend_callbacks(void);
    void reset_callbacks(void);
};

}
}