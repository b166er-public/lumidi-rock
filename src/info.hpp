#pragma once

#include <vector>
#include <string>

namespace lumidi {
namespace info {

unsigned int get_input_device_count(void);
unsigned int get_output_device_count(void);
std::vector<std::string> get_input_device_names(void);
std::vector<std::string> get_output_device_names(void);

unsigned long get_timestamp(void);

void sigint_handler(int signal);
bool has_caught_sigint_event(void);

void sleep(int milliseconds);

}
}