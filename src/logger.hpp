#pragma once

#include <string>
#include <mutex>
#include <chrono>

namespace lumidi {
namespace utils {

class Logger {

private:
    std::string name;
    std::mutex io_mutex;
    std::chrono::high_resolution_clock::time_point start_ts;
    long delta_time(void);

public:
    Logger(std::string name = "");
    void info(std::string msg);
    void warning(std::string msg);
    void error(std::string msg);
};

}
}
