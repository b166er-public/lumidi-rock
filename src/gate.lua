local EXPORTS = {};

engine_api = nil;

local Gate = {
    __name = "Gate",
}

function Gate:new(api)
    local obj = {};

    self.__index = self;
    setmetatable(obj, self);

    obj.engine_api = api;
    obj.is_opened_state = false;
    return obj;
end

function Gate:is_opened()
    return self.is_opened_state;
end

function Gate:is_closed()
    return not self.is_opened_state;
end

function Gate:open()
    self.is_opened_state = true;
end

function Gate:wait_open()
    while true do
        if self.is_opened_state then
            return;
        else
            self.engine_api:sleep(0);
        end
    end
end

function Gate:close()
    self.is_opened_state = false;
end

function Gate:wait_close()
    while true do
        if not self.is_opened_state then
            return;
        else
            self.engine_api:sleep(0);
        end
    end
end


function EXPORTS.create(engine_api_instance)
    if not engine_api then
        engine_api = require("lumidi_engine_api");
    end
    
    if engine_api.instanceof(engine_api_instance) then
        return Gate:new(engine_api_instance)
    else
        error("Please provide an instance of EngineApi as parameter!");
    end
end

function EXPORTS.instanceof(obj)
    return getmetatable(obj) == Gate;
end

return EXPORTS;