lumidi = require("lumidi");

config = lumidi.EngineConfig.create();
config:enable_master_clock(false);
config:add_input_device("in", 1);
config:add_output_device("out", 2);

function drum_layer_1(api)

    local kick_key_ids = {
        lumidi.NoteUtils.note_to_key("C",0),
        lumidi.NoteUtils.note_to_key("E",0),
    }

    local additiona_key_ids = {
        lumidi.NoteUtils.note_to_key("C#",0), -- snare
        lumidi.NoteUtils.note_to_key("D",0) -- hi-hat
    }

    local add_repeating_fn = function(times)
        local repeat_key_id = additiona_key_ids[math.random(1,#additiona_key_ids)];
        local repeating_fn = function(api)
            for i=1,times do
                api:play_note( "out", 0, repeat_key_id, 0.25, 60 );
                api:sleep_beat(0.25);
            end
        end

        api:add_layer("repeating-" .. math.random(1000,9999), repeating_fn);
    end

    local time = 0;
    while true do
        if math.random() > 0.8 then
            add_repeating_fn(math.random(1,4));
        end
        local kick_key_id = kick_key_ids[math.random(1,#kick_key_ids)];

        local velocity = 60;
        if time % 2 == 0 then
            velocity = 80;
        end

        api:play_note( "out", 0, kick_key_id, 0.8, velocity );
        api:sleep_beat_aligned(0);
        time = time + 1;
    end
end

function pitch_bend_sine(api)
    local out_port = api:get_output_port("out");
    
    local iteration_duration_in_beats = 16;
    local iterations_per_cycle = 1000;
    local beats_per_iteration = iteration_duration_in_beats / iterations_per_cycle;
    local iteration_number = 0;
    local pb_max = (2^14)-1;
    
    while true do
        local phase = 2 * math.pi / iterations_per_cycle * iteration_number;
        local pb_value_normalized = (0.5 * math.sin(phase)) + 0.5;
        local pb_value = math.floor(pb_value_normalized * pb_max);
        -- out_port:set_pitch_bend_value(1, pb_value);
        out_port:set_high_resolution_controller_value(0, 1, pb_value);
        iteration_number = (iteration_number + 1) % iterations_per_cycle;
        api:sleep_beat(beats_per_iteration);
    end
end

-- config:add_layer("drum_layer_1",drum_layer_1);
config:add_layer("pitch_bend_sine",pitch_bend_sine);

engine = lumidi.Engine.create(config);
engine:start();
