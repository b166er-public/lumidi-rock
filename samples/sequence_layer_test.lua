lumidi = require("lumidi");

config = lumidi.EngineConfig.create();
config:enable_master_clock(false);
config:add_input_device("in", 1);
config:add_output_device("out", 2);

function arpeggiator1(api)
    local in_port = api:get_input_port("in");
    local out_port = api:get_output_port("out");
    while true do
        local active_keys = in_port:list_activated_keys_now(0);
        if #active_keys > 0 then

            -- Get chord
            local chord_value = in_port:get_controller_value(0,35) / 128.0;
            local oct1 = 3;
            local oct2 = 12;
            if chord_value < 0.25 then
                oct1 = 12;
                oct2 = 24;
            elseif chord_value < 0.5 then
                oct1 = 12;
                oct2 = 19;
            elseif chord_value < 0.75 then
                oct1 = 9;
                oct2 = 15;
            else
                oct1 = 9;
                oct2 = 24;
            end

            -- Get speed
            local chord_speed = 0.125 + (in_port:get_controller_value(0,37) / 128.0);

            -- Play chord
            local base_note = active_keys[#active_keys];
            api:play_note( "out", 0, base_note, chord_speed, 60 );
            api:play_note( "out", 0, base_note + oct1, chord_speed, 60 );
            api:play_note( "out", 0, base_note + oct2, chord_speed, 60 );
        else
            api:sleep_beat_aligned(0);
        end
    end
end

function create_beat_layer(api)
    local base_note = lumidi.NoteUtils.note_to_key("C", 4);
    local chord_1 = lumidi.ChordUtils.chord_to_intervals("Major");
    local chord_2 = lumidi.ChordUtils.chord_to_intervals("m");
    local chord_3 = lumidi.ChordUtils.chord_to_intervals("dim");
    local keys = {}
    local durations = { 0.25, 0.5 };
    local velocities = { 30, 60 };

    for _, note in ipairs(chord_1) do table.insert(keys, note); end
    for _, note in ipairs(chord_2) do table.insert(keys, note); end
    for _, note in ipairs(chord_3) do table.insert(keys, note); end

    local seq_layer = api:create_sequence_layer(
        "out",
        0,
        keys,
        durations,
        velocities,
        false
    );

    seq_layer:activate();

    local seq_layer_pg = seq_layer:get_play_gate();
    
    local seq_layer_ctrl_fn = function (api)
        local in_port = api:get_input_port("in");

        while true do
            local active_keys = in_port:list_activated_keys_now(0);
            if #active_keys > 0 then
                if seq_layer_pg:is_closed() then
                    seq_layer_pg:open();
                else
                    api:sleep_beat_aligned(0);
                end
            else
                if seq_layer_pg:is_opened() then
                    seq_layer_pg:close();
                else
                    api:sleep_beat_aligned(0);
                end
            end
        end
    end

    api:add_layer("seq-layer-ctrl-layer", seq_layer_ctrl_fn);
end

config:add_layer("create_beat_layer", create_beat_layer);
config:add_layer("arpeggiator1",arpeggiator1);

engine = lumidi.Engine.create(config);
engine:start();
