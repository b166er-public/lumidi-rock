lumidi = require("lumidi");

config = lumidi.EngineConfig.create();
config:enable_master_clock(true);
config:add_input_device("in", 1);
--config:add_input_device("in_tronic", 3);
config:add_output_device("out", 2);
config:set_bpm(50);

-- Beat layers

local chn_drums = 0;

function beat_slow(api)
    local chn_drums = 0;
    local kick_key_id = lumidi.NoteUtils.note_to_key("C", 0);
    while true do
        api:play_note("out", chn_drums, kick_key_id, 0.5, 120);

        if math.random() > 0.8 then
            api:play_note("out", chn_drums, kick_key_id, 0.1, 120);
            api:sleep_beat(0.15);
            api:play_note("out", chn_drums, kick_key_id, 0.1, 120);
        end
        api:sleep_beat_aligned(3);
    end
end

function beat_medium(api)
    local chn_drums = 0;
    local rim_key_id = lumidi.NoteUtils.note_to_key("F", 0);
    while true do
        api:sleep_beat(0.25);
        api:play_note("out", chn_drums, rim_key_id, 0.5, 60);
        api:sleep_beat_aligned(2);
    end
end

function beat_fast(api)
    local percussion_key_id = lumidi.NoteUtils.note_to_key("F#", 0);
    while true do
        if math.random() > 0.95 then
            for i=1,7 do
                api:play_note("out", chn_drums, percussion_key_id, 0.1, 120);
                api:sleep_beat(0.1);  
            end
        else
            api:play_note("out", chn_drums, percussion_key_id, 0.5, 80);
        end
        api:sleep_beat_aligned(0);
    end
end

-- Melodic layer

function melodic_arp_1(api)
    local melodic_chn = 1;
    local base_note = lumidi.NoteUtils.note_to_key("C", 3);
    local chord = {};
    for _, note in ipairs( lumidi.ChordUtils.chord_to_intervals("Major") ) do table.insert(chord, note); end
    for _, note in ipairs( lumidi.ChordUtils.chord_to_intervals("m") ) do table.insert(chord, note); end
    for _, note in ipairs( lumidi.ChordUtils.chord_to_intervals("m",3) ) do table.insert(chord, note); end
    for _, note in ipairs( lumidi.ChordUtils.chord_to_intervals("m",7) ) do table.insert(chord, note); end
    for _, note in ipairs( lumidi.ChordUtils.chord_to_intervals("dim") ) do table.insert(chord, note); end

    local note_duration_percent = 0.9;
    local note_rest_percent = 1.0 - note_duration_percent;
    local cycles_per_beat = 4;

    local note_duration_beats = 1.0 / cycles_per_beat * note_duration_percent;
    local note_rest_beats = 1.0 / cycles_per_beat * note_rest_percent;

    local octave_interval = 3;

    local iteration = 0;
    local chord_idx = 0;
    while true do
        local c_key_id = base_note + chord[ (chord_idx % #chord) + 1] + 12 * (iteration % octave_interval);
        api:play_note(
            "out", 
            melodic_chn, 
            c_key_id, 
            note_duration_beats, 
            100
        );

        if iteration % (cycles_per_beat * octave_interval) == 0 then
            api:sleep_beat_aligned(0);
        else
            api:sleep_beat(note_rest_beats);
        end

        iteration = iteration + 1;

        if iteration % 24 == 0 then
            chord_idx = chord_idx + 1;
        end
    end
end

-- Mod wheel

function mod_wheel(api)
    local out_port = api:get_output_port("out");
    
    local iteration_duration_in_beats = 16;
    local iterations_per_cycle = 1000;
    local beats_per_iteration = iteration_duration_in_beats / iterations_per_cycle;
    local iteration_number = 0;
    local pb_max = (2^14)-1;
    
    while true do
        local phase = 2 * math.pi / iterations_per_cycle * iteration_number;
        local pb_value_normalized = (0.5 * math.sin(phase)) + 0.5;
        local pb_value = math.floor(pb_value_normalized * pb_max);
        out_port:set_high_resolution_controller_value(1, 1, pb_value);
        iteration_number = (iteration_number + 1) % iterations_per_cycle;
        api:sleep_beat(beats_per_iteration);
    end
end

-- Pumper depth

function stereo_width_cc(api)
    local out_port = api:get_output_port("out");
    local cc_id_1 = 20;
    local cc_id_2 = 21;
    local cc_val_min = 0;
    local cc_val_max = 100;
    local hold_duration_beats = 0.25;
    local melodic_chn = 1;
    while true do
        out_port:set_controller_value(
            melodic_chn,
            cc_id_1,
            math.random(cc_val_min, cc_val_max)
        );
        out_port:set_controller_value(
            melodic_chn,
            cc_id_2,
            math.random(cc_val_min, cc_val_max)
        );
        api:sleep_beat(hold_duration_beats);
    end
end

-- Playtronic -> Mellotron

function playtronic_to_mellotron(api)
    local in_port = api:get_input_port("in_tronic");
    local out_port = api:get_output_port("out");
    local chord = lumidi.ChordUtils.chord_to_intervals("m");
    local mellotron_chn_id = 2;
    while true do
        local ck = in_port:last_activated_key(0);
        if ck then
            out_port:set_key_value(mellotron_chn_id, ck + chord[1], 100);
            api:sleep_beat(0.1);
            for i=1,3 do
                out_port:set_key_value(mellotron_chn_id, ck + chord[2], 100);
                out_port:set_key_value(mellotron_chn_id, ck + chord[3], 100);
                api:sleep_beat(0.1);
                out_port:set_key_value(mellotron_chn_id, ck + chord[2], 0);
                out_port:set_key_value(mellotron_chn_id, ck + chord[3], 0);
                api:sleep_beat(0.1);
            end
            out_port:set_key_value(mellotron_chn_id, ck + chord[1], 0);
            api:sleep_beat(0.1);
        else
            api:sleep(0);
        end
    end
end

config:add_layer("beat_slow",beat_slow);
config:add_layer("beat_medium",beat_medium);
config:add_layer("beat_fast",beat_fast);

config:add_layer("melodic_arp_1",melodic_arp_1);
config:add_layer("mod_wheel",mod_wheel);

config:add_layer("stereo_width_cc",stereo_width_cc);

config:add_layer("playtronic_to_mellotron",playtronic_to_mellotron);


engine = lumidi.Engine.create(config);
engine:start();
