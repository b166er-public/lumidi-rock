lumidi = require("lumidi");

config = lumidi.EngineConfig.create();
config:enable_master_clock(false);
config:add_input_device("in", 1);
config:add_output_device("out", 2);

function arpeggiator1(api)
    local in_port = api:get_input_port("in");
    local out_port = api:get_output_port("out");
    while true do
        local active_keys = in_port:list_activated_keys_now(0);
        if #active_keys > 0 then

            -- Get chord
            local chord_value = in_port:get_controller_value(0,35) / 128.0;
            local oct1 = 3;
            local oct2 = 12;
            if chord_value < 0.25 then
                oct1 = 12;
                oct2 = 24;
            elseif chord_value < 0.5 then
                oct1 = 12;
                oct2 = 19;
            elseif chord_value < 0.75 then
                oct1 = 9;
                oct2 = 15;
            else
                oct1 = 9;
                oct2 = 24;
            end

            -- Get speed
            local chord_speed = 0.125 + (in_port:get_controller_value(0,37) / 128.0);

            -- Play chord
            local base_note = active_keys[#active_keys];
            out_port:set_key_value(0,base_note,60);
            api:sleep_beat(chord_speed);
            out_port:set_key_value(0,base_note,0);
            out_port:set_key_value(0,base_note + oct1,60);
            api:sleep_beat(chord_speed);
            out_port:set_key_value(0,base_note + oct1,0);
            out_port:set_key_value(0,base_note + oct2,60);
            api:sleep_beat(chord_speed);
            out_port:set_key_value(0,base_note + oct2,0);
        else
            api:sleep(0);
        end
    end
end

function controller_monitor(api)
    local in_port = api:get_input_port("in");
    while true do
        local t0 = api:get_timestamp();
        api:sleep_beat(1);
        local ctrl_changed = in_port:list_modified_controllers_after(0,t0);
        for index, ctrl_id in ipairs(ctrl_changed) do
            local ctrl_value = in_port:get_controller_value(0,ctrl_id);
            print("CTRL [ " .. ctrl_id .. " ] -> " .. ctrl_value);
        end
    end
end

config:add_layer("apr1", arpeggiator1);
config:add_layer("ctrl_monitor", controller_monitor);

engine = lumidi.Engine.create(config);
engine:start();
