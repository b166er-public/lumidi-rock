lumidi = require("lumidi");

config = lumidi.EngineConfig.create();
config:enable_master_clock(true);
config:add_input_device("in", 1);
config:add_output_device("out", 2);

function arp1(api)
    local in_port = api:get_input_port("in");

    local chords = {
        lumidi.ChordUtils.chord_to_intervals("Major"),
        lumidi.ChordUtils.chord_to_intervals("m"),
        lumidi.ChordUtils.chord_to_intervals("maj7"),
        lumidi.ChordUtils.chord_to_intervals("dim"),
    };
    local chord_id = 1;


    while true do
        local active_key = in_port:get_key_activation_counter(
            0,
            lumidi.NoteUtils.note_to_key("C",0)
        );

        if active_key % 2 == 1 then

            local base_note = lumidi.NoteUtils.note_to_key("C", 4);
            
            local change_chord = math.random() > 0.7;
            if change_chord then
                chord_id = math.random(1, #chords);
            end

            -- Play chord
            api:play_note( "out", 0, base_note + chords[chord_id][1], 1.0, 60 );
            api:sleep_beat_aligned(0);
            api:play_note( "out", 0, base_note + chords[chord_id][2], 1.0, 60 );
            api:sleep_beat_aligned(0);
            api:play_note( "out", 0, base_note + chords[chord_id][3], 1.0, 60 );
            api:sleep_beat_aligned(0);
        else
            api:sleep_beat_aligned(0);
        end
    end
end

function random_beats(api)
    local in_port = api:get_input_port("in");
    local chn_id_drums = 1;
    while true do
        local active_key = in_port:get_key_activation_counter(
            0,
            lumidi.NoteUtils.note_to_key("C#",0)
        );

        if active_key % 2 == 1 then
            local kick_note_id = lumidi.NoteUtils.note_to_key("C", 0);
            local rnd = math.random(0,5);

            if (rnd >= 0) and (rnd < 3) then
                api:sleep_beat_aligned(0);
            elseif rnd == 3 then
                api:play_note( "out", chn_id_drums, kick_note_id, 0.9, 120 );
                api:sleep_beat_aligned(0);
            elseif rnd == 4 then
                api:play_note( "out", chn_id_drums, kick_note_id, 0.3, 80 );
                api:sleep_beat(0.1);
                api:play_note( "out", chn_id_drums, kick_note_id, 0.3, 80 );
                api:sleep_beat_aligned(0);
            elseif rnd == 5 then
                api:play_note( "out", chn_id_drums, kick_note_id, 0.1, 40 );
                api:sleep_beat(0.1);
                api:play_note( "out", chn_id_drums, kick_note_id, 0.1, 110 );
                api:sleep_beat(0.1);
                api:play_note( "out", chn_id_drums, kick_note_id, 0.1, 40 );
                api:sleep_beat_aligned(0);
            end
        else
            api:sleep_beat_aligned(0);
        end
    end
end

function random_beats_2(api)
    local in_port = api:get_input_port("in");
    local chn_id_drums = 1;
    while true do
        local active_key = in_port:get_key_activation_counter(
            0,
            lumidi.NoteUtils.note_to_key("D",0)
        );

        if active_key % 2 == 1 then
            local clap_note_id = lumidi.NoteUtils.note_to_key("E", 0);
            api:play_note( "out", chn_id_drums, clap_note_id, 0.8, 60 );
            api:sleep_beat_aligned(0);
            api:play_note( "out", chn_id_drums, clap_note_id, 0.8, 120 );
            api:sleep_beat_aligned(0);
        else
            api:sleep_beat_aligned(0);
        end
    end
end

function fast_pulse_piano(api)
    local in_port = api:get_input_port("in");
    local chn_id_piano = 2;
    while true do
        local active_keys = in_port:list_activated_keys_now(
            0,
            lumidi.NoteUtils.note_to_key("C", 1),
            lumidi.NoteUtils.note_to_key("B", 1)
        );
        if #active_keys > 0 then
            local base_note = active_keys[#active_keys] + 12*3;
            local beats_per_note = 0.1;
            local velocity = 1.0;
            local dimming = 0.9;

            for i=1,10 do
                api:play_note( 
                    "out", 
                    chn_id_piano, 
                    base_note, 
                    beats_per_note, 
                    math.floor(velocity * 120.0)
                );
                api:sleep_beat(beats_per_note / 2.0);
                velocity = velocity * dimming;
            end
            
        else
            api:sleep(0);
        end
    end
end

function fast_pulse_piano_2(api)
    local in_port = api:get_input_port("in");
    local chn_id_piano_2 = 3;
    while true do
        local active_keys = in_port:list_activated_keys_now(
            0,
            lumidi.NoteUtils.note_to_key("C", 2),
            lumidi.NoteUtils.note_to_key("B", 2)
        );
        if #active_keys > 0 then
            local base_note = active_keys[#active_keys] + 12*2;
            local beats_per_note = 0.1;
            local velocity = 1.0;
            local dimming = 0.9;

            for i=1,10 do
                api:play_note( 
                    "out", 
                    chn_id_piano_2, 
                    base_note, 
                    beats_per_note, 
                    math.floor(velocity * 120.0)
                );
                api:sleep_beat(beats_per_note / 2.0);
                velocity = velocity * dimming;
            end
            
        else
            api:sleep(0);
        end
    end
end

config:add_layer("arp1",arp1);
config:add_layer("random_beats",random_beats);
config:add_layer("random_beats_2",random_beats_2);
config:add_layer("fast_pulse_piano",fast_pulse_piano);
config:add_layer("fast_pulse_piano_2",fast_pulse_piano_2);

engine = lumidi.Engine.create(config);
engine:start();
