#!/bin/bash

rm -rf ./build
find . -name "*.o" -type f -delete
find . -name "*.so" -type f -delete