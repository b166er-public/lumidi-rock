# lumidi-rock

## Build GDB

```bash
./configure --prefix=/home/dm-1/apps/gdb/12-1/build --with-python=/usr/bin/python3
```


## Building lua 5.4.4

```bash
make install INSTALL_TOP=/home/dm-1/apps/lua/5-4-4
```

## Building luarocks 3.9.1

```bash
./configure --with-lua-bin=/home/dm-1/apps/lua/5-4-4/bin --prefix=/home/dm-1/apps/luarocks/3-9-1 
make
make install
```

## Before starting

```bash
source activate.source
```

## List all installed files from package

```bash
apt list --installed | grep "midi"
dpkg -L librtmidi-dev
```

## List shared libraries currently in use by process

```bash
lsof -p <pid>
```

## Test native library

```bash
nm = require("lumidi_native_info")
nm.get_input_device_count()
nm.get_output_device_count()
dev_names = nm.get_input_device_names()
nm.get_timestamp()
for k,v in pairs(dev_names) do print(k .. " -> " .. v .. "\n") end
```

## Test MidiInDevice

```bash
nm = require("lumidi_native_info")
mid = require("lumidi_native_midi_in_device")
d2 = mid.create(2)
d2:open()
t1 = nm.get_timestamp()
d2:get_key_value(0,28)
d2:get_key_modified_ts(0,28)
d2:any_changes_after(0,t1)
d2:close()

al1 = d2:list_activated_keys_now(0)
for k,v in pairs(al1) do print(k .. " -> " .. v .. "\n") end
```

## Test MidiOutDevice

```bash
info = require("lumidi_native_info")
mod = require("lumidi_native_midi_out_device")

dev_names = info.get_output_device_names()
for k,v in pairs(dev_names) do print(k .. " -> " .. v .. "\n") end

dev = mod.create(1)
dev:open()
dev:set_key_value(0,28,60)
dev:set_key_value(0,30,60)
dev:set_key_value(0,33,60)
```

## Test Engine Config

```bash
lumidi = require("lumidi");
config = lumidi.EngineConfig.create()
config2 = lumidi.EngineConfig.create()

cfg = lumidi.EngineConfig.create();
e = lumidi.Engine.create(cfg);
e:start();
```