#!/bin/bash

mkdir -p build
luarocks --tree build path > ./build/paths.source
source ./build/paths.source
printenv > ./build/variables.env